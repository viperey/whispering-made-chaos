1
00:00:00,000 --> 00:00:16,000
Good evening. There was a man called Galileo Galilei.

2
00:00:16,000 --> 00:00:23,000
Dedicated to the study, now locked up, watching stars,

3
00:00:23,000 --> 00:00:33,000
drawing the conclusions of his observation, he discovered that the Earth was not in the center of the universe,

4
00:00:33,000 --> 00:00:42,000
that it was moving, and therefore it was the Sun that occupied the center and around which the planets and among them the Earth rotated.

5
00:00:42,000 --> 00:00:55,000
That discovery faced the institutionalized truth. The Vatican, the Church, the popular beliefs of the moment,

6
00:00:55,000 --> 00:01:02,000
and the insistence on the maintenance of what he had discovered, cost him to go to trial.

7
00:01:02,000 --> 00:01:14,000
And in front of the accused, how could he think that Aristotle had been wrong? How could he think that the Holy Scriptures were lying?

8
00:01:14,000 --> 00:01:25,000
How could a naive wise man dare to think that he had discovered something that was against what the Magisterium of the Holy Mother Church had been saying for centuries?

9
00:01:25,000 --> 00:01:34,000
And above all, did the people not clamor against the one who dared to question the centrality of the planet Earth?

10
00:01:34,000 --> 00:01:43,000
The pressures are tremendous. It has almost to be abjured. But in a moment, in the last rebellion,

11
00:01:43,000 --> 00:01:55,000
and muttering with a smile to the Saramago, soft but firm, he says in the native Italian,

12
00:01:55,000 --> 00:02:06,000
and yet it moves. Because the mathematical calculations, the observations, the exercise of reason,

13
00:02:06,000 --> 00:02:14,000
because what his eyes were seeing night after night, were showing him that it was the Earth that was moving.

14
00:02:14,000 --> 00:02:23,000
Well, today we are in the Spain of 1999, in the Europe of 1999, and in the world,

15
00:02:23,000 --> 00:02:33,000
in a moment in which, on other occasions in history, societies have had to choose one way or another.

16
00:02:33,000 --> 00:02:41,000
Either to continue in the resignation or to face the rebellion that Manolo Cañada just said.

17
00:02:41,000 --> 00:02:53,000
The resignation is a product that, like any drug, sleeps the people. It sleeps their consciousness.

18
00:02:53,000 --> 00:03:02,000
The resignation is like morphine, cocaine or heroin. The resignation is the product of many causes.

19
00:03:02,000 --> 00:03:12,000
I'm going to list a few. The resignation is the daughter of that totalizing speech, as if it were a new religion.

20
00:03:12,000 --> 00:03:20,000
There is no more truth than competitiveness. There are no more saints or powers than the markets.

21
00:03:20,000 --> 00:03:30,000
The economy has to grow constantly, no matter if the waters are polluted, the rivers, the seas or the air.

22
00:03:30,000 --> 00:03:37,000
Competitiveness, sustained growth and the markets. That is the only thing that matters.

23
00:03:37,000 --> 00:03:48,000
Its power cannot be contested. And besides, it shows us the existence of the societies themselves that this is what produces well-being.

24
00:03:48,000 --> 00:03:55,000
And it doesn't matter that the people of the street see that this well-being has not reached the son or the daughter,

25
00:03:55,000 --> 00:04:03,000
that they have to go to the temporary work company that charges them 40% of the payroll to put them in a company.

26
00:04:03,000 --> 00:04:10,000
It doesn't matter that the person who still has a pension, who does not reach the minimum interprofessional salary and is almost half,

27
00:04:10,000 --> 00:04:18,000
69,000 and a bit of pesetas, half of that, sometimes does not reach. It doesn't matter the retirement of the one who entered at 45 years old.

28
00:04:18,000 --> 00:04:26,000
It doesn't matter that the woman, mother and wife, but who also has to work, does not charge the same as the man doing the same task,

29
00:04:26,000 --> 00:04:36,000
violating entire articles of the Fundamental Charter of the United Nations and the Universal Declaration of Human Rights and text of the Spanish Constitution.

30
00:04:36,000 --> 00:04:46,000
It doesn't matter. Because they are saying that there is no better than competitiveness, how well we live, how well we go, the data, the figures.

31
00:04:46,000 --> 00:04:54,000
It doesn't matter that people see or want to see around them the facts that are contradicting that message.

32
00:04:54,000 --> 00:05:01,000
Because so that it is not seen or so that it is less hurtful, there are succedaneos.

33
00:05:01,000 --> 00:05:10,000
There you have the television, football, a lot of football, more football than in previous times in the history of Spain.

34
00:05:10,000 --> 00:05:19,000
There you have degrading contests that do not feed the reason, the study, the analysis.

35
00:05:19,000 --> 00:05:28,000
There you have the life of the popular characters that are dissected and opened so that we bite as if we were scavenger birds

36
00:05:28,000 --> 00:05:33,000
and forgetting the environment we have, we enter what happens in their alcoves.

37
00:05:33,000 --> 00:05:47,000
There is a whole literature of evasion so that people do not see, do not see and therefore confuse their real existence with the existence that they put on the screens or in the news.

38
00:05:47,000 --> 00:05:57,000
So that it happens like what I say so many times about the old woman who at the end of the 19th century was selling cereal at the door of the Palace of the Opera of Madrid

39
00:05:57,000 --> 00:06:03,000
in a month of January at two in the morning, cold and wrapped in a towel selling cereals to be able to survive.

40
00:06:03,000 --> 00:06:11,000
And when men and women entered wrapped in garments, in layers with luxury and jewelry, she said how well we live in Madrid.

41
00:06:11,000 --> 00:06:19,000
A case of alienation, a case of supplantation, a case of drug addiction.

42
00:06:19,000 --> 00:06:28,000
The image, how well we live, the stories of the alcove, the magazines of the heart, the frivolities that make us forget what happens daily.

43
00:06:28,000 --> 00:06:33,000
Or if it is seen, it is raised to another category as if it were not the real thing.

44
00:06:33,000 --> 00:06:40,000
Resignation also because the official discourse that goes down from many places,

45
00:06:40,000 --> 00:06:53,000
it goes down from the public authorities, it goes down from the sentences of the courts, from the cathedrals, from the classes of EGB where the school teacher is already injecting certain ideas.

46
00:06:53,000 --> 00:07:00,000
It goes down from television and the media, the discourse that there is no other way out, this is the only possible thing.

47
00:07:00,000 --> 00:07:04,000
And if not, look, we are wrong, but worse they were on the Berlin Wall.

48
00:07:04,000 --> 00:07:13,000
And when it comes to talk about the Berlin Wall, it is because they are not right and you have to say, look how bad they were, because it is the only justification.

49
00:07:13,000 --> 00:07:21,000
Resignation because the people, when they have problems, they are not rebels.

50
00:07:21,000 --> 00:07:29,000
The one who has to eat every day can not afford the luxury of losing for an act of rebellion the job.

51
00:07:29,000 --> 00:07:35,000
Rebellion has always arisen from those who ate every day.

52
00:07:35,000 --> 00:07:48,000
From here, the great guilt of many Spanish intellectuals, who eating every day either of the manger or their work, have not been able to say enough to this situation of degradation.

53
00:07:48,000 --> 00:08:01,000
From there, a resignation that is born from the daily evidence of unemployment, which is true.

54
00:08:01,000 --> 00:08:09,000
Of that unemployment that they say is reduced because the statistics say that when a person works two hours a week is no longer paid.

55
00:08:09,000 --> 00:08:21,000
A statistical decrease of part-time jobs, of the extraordinary hours that are imposed but are not charged, of the anguish if tomorrow to be able to work, that is resignation.

56
00:08:21,000 --> 00:08:32,000
Resignation that falls on a people that realizes, or does not realize, because they do not like it or do not want to see it, or do not let see it, that we are going backwards.

57
00:08:32,000 --> 00:08:41,000
That we are reaching our own rates of the 19th century, that social security for all, that the issue of sustainability is employment continuously going down,

58
00:08:41,000 --> 00:08:46,000
on account of the Declaration of the Universal of Human Rights, or of the Constitution itself.

59
00:08:46,000 --> 00:08:53,000
Resignation that arises from the guilt of the self-employed.

60
00:08:53,000 --> 00:09:04,000
One of the successes of the American system is to get the poor, the miserable, to feel guilty of their situation.

61
00:09:04,000 --> 00:09:08,000
It is the Calvinist philosophy, daughter of Protestantism.

62
00:09:08,000 --> 00:09:16,000
You are guilty of your situation, you have not been able to succeed, that is the philosophy of American society.

63
00:09:16,000 --> 00:09:19,000
And if you have not succeeded, it is because you are responsible.

64
00:09:19,000 --> 00:09:24,000
This society gives opportunities to everyone, if you have not been able to do so, you are guilty.

65
00:09:24,000 --> 00:09:30,000
And then, the oppressed, the poor, the slave, takes responsibility for their situation.

66
00:09:30,000 --> 00:09:33,000
It is perfect the domain of power.

67
00:09:33,000 --> 00:09:40,000
A domain of power that is no longer based on force, coercion, the use of the civil guard or the army.

68
00:09:40,000 --> 00:09:45,000
It is based on a much more terrible domain, harder, the domain of the mind.

69
00:09:45,000 --> 00:09:52,000
That opium that falls from the television sets, that opium that falls from the sentences of the courts,

70
00:09:52,000 --> 00:09:56,000
from the political speeches, that soaks the mentality of the people and says,

71
00:09:56,000 --> 00:10:00,000
shut up, shut up, shut up, because if you do not shut up, it can be worse.

72
00:10:00,000 --> 00:10:08,000
That is the resignation that occurs as a consequence of feeling that stop, that he is the author of his situation.

73
00:10:08,000 --> 00:10:15,000
And therefore, that comrade who has been accused of charging once, unfairly, the unemployment insurance,

74
00:10:15,000 --> 00:10:17,000
miserable, you are the culprit.

75
00:10:17,000 --> 00:10:22,000
It does not matter that the thieves of high copete are exhibited as brilliant figures,

76
00:10:22,000 --> 00:10:24,000
to teach the children as an example to follow.

77
00:10:24,000 --> 00:10:28,000
But the miserable who has stolen only one month of unemployment insurance,

78
00:10:28,000 --> 00:10:31,000
is the culprit of everything that is happening.

79
00:10:31,000 --> 00:10:33,000
That is resignation.

80
00:10:33,000 --> 00:10:37,000
Resignation that arises from the media.

81
00:10:37,000 --> 00:10:40,000
And do not get mad at me in the cameras, it does not go with you.

82
00:10:40,000 --> 00:10:43,000
But it goes with those who have the power over these companies.

83
00:10:43,000 --> 00:10:47,000
It goes against those who choose to tell the people a part of the truth.

84
00:10:47,000 --> 00:10:51,000
Resignation that consists of giving a single creed.

85
00:10:51,000 --> 00:10:56,000
Say all amen to competitiveness, to the single currency, we are better than ever.

86
00:10:56,000 --> 00:10:58,000
Amen, amen, amen.

87
00:10:58,000 --> 00:11:02,000
It is the chorus like a litany that informs the thought,

88
00:11:02,000 --> 00:11:09,000
that is making beings totally equal, as described what could be the future Orwell in 1984.

89
00:11:09,000 --> 00:11:18,000
That resignation, therefore, is the daughter of an economy, of a political system that confuses many things.

90
00:11:18,000 --> 00:11:26,000
An information that is making arise in our universities, in our institutes, in our academies,

91
00:11:26,000 --> 00:11:32,000
in the basic schools the culture of yes or no, typical of the computer.

92
00:11:32,000 --> 00:11:37,000
Life is full of colors, of tones, and therefore language is much more alive

93
00:11:37,000 --> 00:11:40,000
when there are more things to be described.

94
00:11:40,000 --> 00:11:46,000
Yes or no, white or black, right or left, you answer like the computer.

95
00:11:46,000 --> 00:11:50,000
Affirmative, negative, affirmative, negative.

96
00:11:50,000 --> 00:11:56,000
It is not looking for the thinking human being, capable of reflection, doubt and concern.

97
00:11:56,000 --> 00:11:58,000
It is looking for the thoughtless slave.

98
00:11:58,000 --> 00:12:03,000
And that's why history is not loved, and that's why memory is disdained.

99
00:12:03,000 --> 00:12:06,000
Because human beings are children of memory.

100
00:12:06,000 --> 00:12:11,000
I am what I am because I lived with my parents, my memories, my story, my experiences.

101
00:12:11,000 --> 00:12:15,000
I am the update of a whole past that is alive.

102
00:12:15,000 --> 00:12:18,000
If they take away my memory, I am a zombie, a living dead.

103
00:12:18,000 --> 00:12:21,000
And we want people of living dead,

104
00:12:21,000 --> 00:12:24,000
who are stimulated by the last game of the Marceau-Badrí,

105
00:12:24,000 --> 00:12:28,000
who are stimulated by the last story of such or such count or such or such lady,

106
00:12:28,000 --> 00:12:31,000
who arrive in the corridors, even in the parliaments,

107
00:12:31,000 --> 00:12:34,000
and in the places where they had to debate the problems,

108
00:12:34,000 --> 00:12:38,000
they tell jokes of private life to forget the tremendous reality.

109
00:12:38,000 --> 00:12:44,000
Escapism, drug, just like heroin, just like cocaine, drug, escapism.

110
00:12:44,000 --> 00:12:48,000
Give up the thought, annihilate the critical spirit,

111
00:12:48,000 --> 00:12:51,000
and, therefore, encourage resignation.

112
00:12:51,000 --> 00:12:54,000
And frivolity, a lot of frivolity.

113
00:12:54,000 --> 00:12:58,000
And, therefore, the policy understood as buy-sell of votes.

114
00:12:58,000 --> 00:13:01,000
It does not matter what the people wants.

115
00:13:01,000 --> 00:13:05,000
To the people to which, conveniently, is going to say what they want,

116
00:13:05,000 --> 00:13:08,000
through certain means. More football? Well, more football.

117
00:13:08,000 --> 00:13:12,000
But what I think is not, is that you have to say what the people like.

118
00:13:12,000 --> 00:13:15,000
To which I, through very fine means of communication,

119
00:13:15,000 --> 00:13:18,000
I tell them what is convenient for them.

120
00:13:18,000 --> 00:13:21,000
But I represent a project. I want to explain a project.

121
00:13:21,000 --> 00:13:24,000
I want to watch my people, which I am part of,

122
00:13:24,000 --> 00:13:27,000
to tell them the point of view of our organization.

123
00:13:27,000 --> 00:13:29,000
No, no, no. What is convenient is that you win votes.

124
00:13:29,000 --> 00:13:32,000
That is not well said. You have to be respectable.

125
00:13:32,000 --> 00:13:35,000
You have to speak and say it politically correct,

126
00:13:35,000 --> 00:13:39,000
in a good tone, like the boy from the bourgeoisie of the 19th century.

127
00:13:39,000 --> 00:13:42,000
No, that is not done. That is not said. You do it undercover.

128
00:13:42,000 --> 00:13:45,000
Because everything must remain as it is. Nothing will happen here.

129
00:13:45,000 --> 00:13:48,000
That is to say, the culture of hypocrisy.

130
00:13:48,000 --> 00:13:51,000
To create a hypocritical society, which lies knowingly,

131
00:13:51,000 --> 00:13:54,000
which knows that it is saying something that nobody believes.

132
00:13:54,000 --> 00:13:57,000
But the important thing is not to say it.

133
00:13:57,000 --> 00:14:00,000
The important thing is to do it, but not to say it.

134
00:14:00,000 --> 00:14:03,000
And that cancer is advancing, degrading,

135
00:14:03,000 --> 00:14:08,000
corrupting and annihilating the forces to fight.

136
00:14:08,000 --> 00:14:12,000
And that is a path, without a doubt, a sweet path.

137
00:14:12,000 --> 00:14:16,000
It is the slow death, as a brazier is consumed,

138
00:14:16,000 --> 00:14:20,000
as those who drink the cicuta die,

139
00:14:20,000 --> 00:14:23,000
the death that was given to the great Socrates.

140
00:14:23,000 --> 00:14:26,000
The whole organism is sleeping,

141
00:14:26,000 --> 00:14:30,000
and one dies with a smile on his lips, but he dies.

142
00:14:30,000 --> 00:14:34,000
And the other path is what Manolo has said,

143
00:14:34,000 --> 00:14:39,000
which is rebellion. But rebellion is not a loud gesture,

144
00:14:39,000 --> 00:14:42,000
it is not a scream, it is not an insult,

145
00:14:42,000 --> 00:14:46,000
it is not a beating, it is not a bad answer.

146
00:14:46,000 --> 00:14:49,000
It is much deeper.

147
00:14:49,000 --> 00:14:52,000
Rebellion is a scream of the intelligence, of the will,

148
00:14:52,000 --> 00:14:55,000
which says, and I will say it in Paladino's novel,

149
00:14:55,000 --> 00:14:58,000
I don't feel like saying yes to this current situation.

150
00:14:58,000 --> 00:15:01,000
Why? Because I don't want to, and I refuse to say yes.

151
00:15:01,000 --> 00:15:04,000
Because I understand that there may be another situation,

152
00:15:04,000 --> 00:15:07,000
and therefore I don't assume this power,

153
00:15:07,000 --> 00:15:10,000
and I don't participate in it, and I fight against it.

154
00:15:10,000 --> 00:15:13,000
And this attitude is an intellectual attitude.

155
00:15:13,000 --> 00:15:16,000
And when I say intellectual, I don't mean a university student,

156
00:15:16,000 --> 00:15:19,000
I mean the mind of any human being.

157
00:15:19,000 --> 00:15:22,000
It is a positioning that is born from the mind and the heart,

158
00:15:22,000 --> 00:15:24,000
from the fire of wanting to change.

159
00:15:24,000 --> 00:15:26,000
This is the fundamental rebellion.

160
00:15:26,000 --> 00:15:29,000
The others are voices, they are screams, they are insults,

161
00:15:29,000 --> 00:15:32,000
they are big nests, give cane to the Roman circus.

162
00:15:32,000 --> 00:15:35,000
No, no. Rebellion is no more and no less than the positioning

163
00:15:35,000 --> 00:15:38,000
with other values and the decision to face it.

164
00:15:38,000 --> 00:15:41,000
Rebellion to say that we do not accept

165
00:15:41,000 --> 00:15:44,000
that competitiveness and the market are the ones

166
00:15:44,000 --> 00:15:47,000
that govern the destinies of societies.

167
00:15:47,000 --> 00:15:50,000
That we understand that there is a Universal Declaration of Human Rights

168
00:15:50,000 --> 00:15:53,000
that has to be fulfilled, and that this means

169
00:15:53,000 --> 00:15:56,000
full employment society, where men and women

170
00:15:56,000 --> 00:15:59,000
are exactly the same, where there are no marginals,

171
00:15:59,000 --> 00:16:02,000
and that it will cost a lot of time and a lot of sacrifice,

172
00:16:02,000 --> 00:16:05,000
but it is beautiful to fight, even to die for that.

173
00:16:05,000 --> 00:16:07,000
Because to die, we have to die.

174
00:16:07,000 --> 00:16:10,000
We die at least fighting for a noble ideal

175
00:16:10,000 --> 00:16:13,000
and not consuming ourselves like a brazier.

176
00:16:13,000 --> 00:16:17,000
And it means that fundamental rebellion

177
00:16:17,000 --> 00:16:20,000
as a human entity,

178
00:16:20,000 --> 00:16:24,000
it means defending with that soft irony,

179
00:16:24,000 --> 00:16:27,000
with that tranquility that Maestro Saramago does,

180
00:16:27,000 --> 00:16:30,000
because it is a glory to see him answering

181
00:16:30,000 --> 00:16:33,000
the journalists with that soft irony,

182
00:16:33,000 --> 00:16:36,000
with that tremendous hardness in the background,

183
00:16:36,000 --> 00:16:39,000
but flexibility in the language.

184
00:16:39,000 --> 00:16:42,000
It means defending that there are values

185
00:16:42,000 --> 00:16:45,000
that must be maintained.

186
00:16:45,000 --> 00:16:48,000
The beautiful value of equality.

187
00:16:48,000 --> 00:16:51,000
As one said, blood is red, and we all have it red.

188
00:16:51,000 --> 00:16:54,000
There is no blue blood.

189
00:16:54,000 --> 00:16:57,000
And also, as another said, all hearts,

190
00:16:57,000 --> 00:17:00,000
except for one section, are on the left.

191
00:17:00,000 --> 00:17:03,000
Therefore, that equality that makes

192
00:17:03,000 --> 00:17:06,000
human beings be born in the same way.

193
00:17:06,000 --> 00:17:09,000
There is an essential equality, not egalitarianism,

194
00:17:09,000 --> 00:17:12,000
and therefore dignity of the person

195
00:17:12,000 --> 00:17:15,000
for being what he is, a person.

196
00:17:15,000 --> 00:17:18,000
And next to equality, freedom.

197
00:17:18,000 --> 00:17:21,000
But speaking of freedom is something very big.

198
00:17:21,000 --> 00:17:24,000
Because freedom is assuming that one has free conscience,

199
00:17:24,000 --> 00:17:27,000
which is not the same as freedom of conscience.

200
00:17:27,000 --> 00:17:30,000
Free conscience means that I can decide

201
00:17:30,000 --> 00:17:34,000
if I have all the elements to formulate my decision.

202
00:17:34,000 --> 00:17:37,000
I am well informed.

203
00:17:37,000 --> 00:17:40,000
I am well trained. I eat every day.

204
00:17:40,000 --> 00:17:43,000
I have a roof where to rest.

205
00:17:43,000 --> 00:17:46,000
I have clothes to put on, and once I have

206
00:17:46,000 --> 00:17:49,000
all the elements satisfied, I can start

207
00:17:49,000 --> 00:17:52,000
to think about being a free man.

208
00:17:52,000 --> 00:17:55,000
Because if I have to look for a job, cheating,

209
00:17:55,000 --> 00:17:58,000
standing in line, selling myself for four pearls

210
00:17:58,000 --> 00:18:01,000
because I have to eat my own,

211
00:18:01,000 --> 00:18:04,000
I am not a free man, even if tomorrow

212
00:18:04,000 --> 00:18:07,000
I am allowed to vote in the polls.

213
00:18:07,000 --> 00:18:10,000
I am driven by my hunger, by my need to sell myself

214
00:18:10,000 --> 00:18:13,000
at every moment for work.

215
00:18:13,000 --> 00:18:16,000
And next to freedom,

216
00:18:16,000 --> 00:18:19,000
in the most splendid sense of the word,

217
00:18:19,000 --> 00:18:22,000
justice.

218
00:18:22,000 --> 00:18:25,000
And I am not talking about courts of justice.

219
00:18:25,000 --> 00:18:28,000
I am talking about that which is so simple

220
00:18:28,000 --> 00:18:31,000
to give to everyone what is theirs,

221
00:18:31,000 --> 00:18:34,000
that the law prevails, that there are no distinctions,

222
00:18:34,000 --> 00:18:37,000
that everyone is measured by the same scale,

223
00:18:37,000 --> 00:18:40,000
by the scale of the law.

224
00:18:40,000 --> 00:18:43,000
That is what can make it possible for people to live together in society

225
00:18:43,000 --> 00:18:46,000
as long as the law is fair

226
00:18:46,000 --> 00:18:49,000
and applies justice to everyone equally. Solidarity.

227
00:18:49,000 --> 00:18:52,000
It is a message that can unite us all.

228
00:18:52,000 --> 00:18:55,000
To all those who spoke of proletarian internationalism,

229
00:18:55,000 --> 00:18:58,000
which is still in force.

230
00:18:58,000 --> 00:19:01,000
To those who speak of the brotherhood of human beings

231
00:19:01,000 --> 00:19:04,000
and because they make reference to their beliefs

232
00:19:04,000 --> 00:19:07,000
based on the theology of liberation.

233
00:19:07,000 --> 00:19:10,000
To those who speak of other assumptions of human liberation.

234
00:19:10,000 --> 00:19:13,000
To other proposals of liberation.

235
00:19:13,000 --> 00:19:16,000
Solidarity, which consists in affirming,

236
00:19:16,000 --> 00:19:19,000
calmly and serenely, that it is not worth fighting for flags,

237
00:19:19,000 --> 00:19:22,000
that the only flag is the currency of the planet Earth

238
00:19:22,000 --> 00:19:25,000
and that humanity is a single race,

239
00:19:25,000 --> 00:19:28,000
a single and unique race, and that it is worth fighting for it.

240
00:19:28,000 --> 00:19:31,000
And this is very important, informed.

241
00:19:31,000 --> 00:19:34,000
Not because it is given a lot of news.

242
00:19:34,000 --> 00:19:37,000
The news is a commodity that is given to be consumed.

243
00:19:37,000 --> 00:19:40,000
The information is a data that is given

244
00:19:40,000 --> 00:19:43,000
so that people think, and from there, extract their consequences.

245
00:19:43,000 --> 00:19:46,000
And from the left,

246
00:19:46,000 --> 00:19:49,000
to speak of austerity.

247
00:19:49,000 --> 00:19:52,000
I particularly like this word.

248
00:19:52,000 --> 00:19:55,000
To speak of austerity was the word

249
00:19:55,000 --> 00:19:58,000
that stifled a speech of Enrico Berlinguer,

250
00:19:58,000 --> 00:20:01,000
that general secretary of the Italian Communist Party

251
00:20:01,000 --> 00:20:04,000
in the tribune. Speaking precisely of austerity.

252
00:20:04,000 --> 00:20:07,000
Austerity in the Roman, Mediterranean sense.

253
00:20:07,000 --> 00:20:10,000
Austerity is not misery.

254
00:20:10,000 --> 00:20:13,000
Austerity means to live with dignity, normally.

255
00:20:13,000 --> 00:20:16,000
Not to waste natural resources.

256
00:20:16,000 --> 00:20:19,000
To possess one thing, if not that things possess one.

257
00:20:19,000 --> 00:20:22,000
Not to go constantly attempting

258
00:20:22,000 --> 00:20:25,000
with nature in a ferocious consumerism.

259
00:20:25,000 --> 00:20:28,000
Austerity means free time to discuss

260
00:20:28,000 --> 00:20:31,000
and dialogue with others. To play.

261
00:20:31,000 --> 00:20:34,000
To make possible the love between beings that know each other.

262
00:20:34,000 --> 00:20:37,000
To live together in the street, in the square, in the Greek Agora.

263
00:20:37,000 --> 00:20:40,000
Austerity means that the best way

264
00:20:40,000 --> 00:20:43,000
to live is to have relations with others in the plane of equality,

265
00:20:43,000 --> 00:20:46,000
feeling free men and women in a democratic society.

266
00:20:46,000 --> 00:20:49,000
Austerity makes us all look

267
00:20:49,000 --> 00:20:52,000
as human beings, and not by our consumption capacity.

268
00:20:52,000 --> 00:20:55,000
I feel like a human being when they say

269
00:20:55,000 --> 00:20:58,000
that a Spaniard consumes so many sausages or so many cars a year.

270
00:20:58,000 --> 00:21:01,000
That is not austerity. That is to measure the human being

271
00:21:01,000 --> 00:21:04,000
by the totality. Austerity, which means

272
00:21:04,000 --> 00:21:07,000
with another word, sobriety.

273
00:21:07,000 --> 00:21:10,000
To speak of concrete things. To speak of things

274
00:21:10,000 --> 00:21:13,000
that are important, even when

275
00:21:13,000 --> 00:21:16,000
language is used to create beauty,

276
00:21:16,000 --> 00:21:19,000
to make us think like our Nobel Prize.

277
00:21:19,000 --> 00:21:22,000
Language is used from sobriety, because words

278
00:21:22,000 --> 00:21:25,000
falling into a cascade, joining together,

279
00:21:25,000 --> 00:21:28,000
constantly recreating themselves, make us think,

280
00:21:28,000 --> 00:21:31,000
make us conceive new ideas, humanize us.

281
00:21:31,000 --> 00:21:34,000
That is austerity, and that is sobriety.

282
00:21:34,000 --> 00:21:37,000
And from there, it is when the speech and the proposal begin,

283
00:21:37,000 --> 00:21:40,000
the pre-employment society,

284
00:21:40,000 --> 00:21:43,000
sustainable development, the distribution of work,

285
00:21:43,000 --> 00:21:46,000
that is, the red, green, violet speech,

286
00:21:46,000 --> 00:21:49,000
the speech of peace. Peace! And peace is not

287
00:21:49,000 --> 00:21:52,000
the absence of war. Peace, for example, is that on the 9th

288
00:21:52,000 --> 00:21:55,000
we are filling up with rocks, because they want to transform

289
00:21:55,000 --> 00:21:58,000
the military base into a super base,

290
00:21:58,000 --> 00:22:01,000
violating the third point of what the Spanish people agreed

291
00:22:01,000 --> 00:22:04,000
in the referendum in 1986.

292
00:22:04,000 --> 00:22:07,000
Peace means that tomorrow, 1,200 men

293
00:22:07,000 --> 00:22:10,000
and Spanish planes that cost money

294
00:22:10,000 --> 00:22:13,000
cannot enter the old Yugoslavia,

295
00:22:13,000 --> 00:22:16,000
because it has not been consulted by the courts

296
00:22:16,000 --> 00:22:20,000
and because Article 62 of the Constitution has been violated again.

297
00:22:20,000 --> 00:22:23,000
It means, therefore, to speak of peace,

298
00:22:23,000 --> 00:22:26,000
peace as justice,

299
00:22:26,000 --> 00:22:29,000
as understanding among equal beings

300
00:22:29,000 --> 00:22:32,000
that are able to reason.

301
00:22:32,000 --> 00:22:35,000
And well, the mechanisms are the same as always.

302
00:22:35,000 --> 00:22:38,000
Mobilization. What is mobilization?

303
00:22:38,000 --> 00:22:41,000
From the left,

304
00:22:41,000 --> 00:22:44,000
mobilization has not always been

305
00:22:44,000 --> 00:22:47,000
just to fill the streets of people,

306
00:22:47,000 --> 00:22:50,000
which also. Mobilization has been

307
00:22:50,000 --> 00:22:53,000
to raise awareness.

308
00:22:53,000 --> 00:22:56,000
We exist, those of us who want to think

309
00:22:56,000 --> 00:22:59,000
on our own,

310
00:22:59,000 --> 00:23:02,000
to disturb others.

311
00:23:02,000 --> 00:23:05,000
If there is any believer here,

312
00:23:05,000 --> 00:23:08,000
I address him or her.

313
00:23:08,000 --> 00:23:11,000
To remind you of the phrase that I explained today

314
00:23:11,000 --> 00:23:14,000
when a person, a colleague

315
00:23:14,000 --> 00:23:17,000
who was a representative of the Theology of Liberation

316
00:23:17,000 --> 00:23:20,000
asked me, and I reminded him of a passage

317
00:23:20,000 --> 00:23:23,000
from the Gospel of my past time, I am a connoisseur.

318
00:23:23,000 --> 00:23:26,000
He said, look, one of the things that appear in the Gospel

319
00:23:26,000 --> 00:23:29,000
is when they ask Jesus of Galilee,

320
00:23:29,000 --> 00:23:32,000
have you come here to bring peace? He says, no,

321
00:23:32,000 --> 00:23:35,000
I have come to bring war. And what did he mean?

322
00:23:35,000 --> 00:23:38,000
I have come to raise awareness, to disturb.

323
00:23:38,000 --> 00:23:41,000
Calm, drugged. We want people who are restless.

324
00:23:41,000 --> 00:23:44,000
We come to disturb, to shake the brain,

325
00:23:44,000 --> 00:23:47,000
to move consciousness. We exist in the measure

326
00:23:47,000 --> 00:23:50,000
that we mobilize thought.

327
00:23:50,000 --> 00:23:53,000
As I said in that church that is in the Orange Bar in Córdoba,

328
00:23:53,000 --> 00:23:56,000
get up and think, it is the most revolutionary thing I have seen

329
00:23:56,000 --> 00:23:59,000
in my life, because the rebellion begins here,

330
00:23:59,000 --> 00:24:02,000
in the head that says, I do not serve, I do not feel like it,

331
00:24:02,000 --> 00:24:05,000
I do not want to assume these values.

332
00:24:05,000 --> 00:24:08,000
It is the mobilization that means, therefore,

333
00:24:08,000 --> 00:24:11,000
that effort to think and to make people think.

334
00:24:11,000 --> 00:24:14,000
The great revolutionaries of history,

335
00:24:14,000 --> 00:24:17,000
the fundamental characteristic was that they made people think.

336
00:24:17,000 --> 00:24:20,000
The revolution was made by people,

337
00:24:20,000 --> 00:24:23,000
the masses, the collectives.

338
00:24:23,000 --> 00:24:26,000
But the value of them is the thought they put in motion.

339
00:24:26,000 --> 00:24:29,000
It is the concept of mobilization,

340
00:24:29,000 --> 00:24:32,000
around the concrete and with the alliances of the whole people.

341
00:24:32,000 --> 00:24:35,000
That's why we make calls,

342
00:24:35,000 --> 00:24:38,000
we want unity, but not to distribute

343
00:24:38,000 --> 00:24:41,000
armchairs, to make transformation programs.

344
00:24:41,000 --> 00:24:44,000
What do we do in the people?

345
00:24:44,000 --> 00:24:47,000
What do we do in the autonomous community? What do we do in Spain?

346
00:24:47,000 --> 00:24:50,000
What do we do in Europe? Alliances.

347
00:24:50,000 --> 00:24:53,000
Alliances between people who basically coincide,

348
00:24:53,000 --> 00:24:56,000
it seems to be, at least theoretically,

349
00:24:56,000 --> 00:24:59,000
that they want to change the world. Let's agree that we can change now,

350
00:24:59,000 --> 00:25:02,000
but change one armchair for another, that's not right anymore.

351
00:25:02,000 --> 00:25:05,000
That's what we do from time immemorial.

352
00:25:05,000 --> 00:25:08,000
And finally, the culture.

353
00:25:08,000 --> 00:25:11,000
The word culture comes from cultivation.

354
00:25:11,000 --> 00:25:14,000
Cultivate, become human every day more.

355
00:25:14,000 --> 00:25:17,000
Culture is not knowing many things.

356
00:25:17,000 --> 00:25:20,000
Culture is capturing

357
00:25:20,000 --> 00:25:23,000
all that humanity has been producing

358
00:25:23,000 --> 00:25:26,000
and that moves us from art

359
00:25:26,000 --> 00:25:29,000
to shudder, to taste beauty,

360
00:25:29,000 --> 00:25:32,000
to understand how humanity

361
00:25:32,000 --> 00:25:35,000
has been overcoming certain problems.

362
00:25:35,000 --> 00:25:38,000
A cult man is not a man who is surrounded

363
00:25:38,000 --> 00:25:41,000
by books, which can also be.

364
00:25:41,000 --> 00:25:44,000
A cult man is a man who looks at the world

365
00:25:44,000 --> 00:25:47,000
with an independent and free gaze.

366
00:25:47,000 --> 00:25:50,000
A cult man can be a peasant from our land.

367
00:25:50,000 --> 00:25:53,000
When Revina, words used in my land,

368
00:25:53,000 --> 00:25:56,000
but he knows how to calculate things, he thinks as he wants.

369
00:25:56,000 --> 00:25:59,000
He is a man who has a type of culture.

370
00:25:59,000 --> 00:26:02,000
And that man, who may not know how to read,

371
00:26:02,000 --> 00:26:05,000
can lend a hand to another cult from the university,

372
00:26:05,000 --> 00:26:08,000
who knows more things, but is in the wave of culture,

373
00:26:08,000 --> 00:26:11,000
because they both converge from their sense of free men

374
00:26:11,000 --> 00:26:14,000
with the ability to think.

375
00:26:14,000 --> 00:26:17,000
And finally, in today's act,

376
00:26:17,000 --> 00:26:20,000
where now the word will be taken by Maestro Saramago,

377
00:26:20,000 --> 00:26:23,000
and said with all affection, in the sense of exercise,

378
00:26:23,000 --> 00:26:26,000
of simplicity and depth,

379
00:26:26,000 --> 00:26:29,000
the voice of Izquierda Unida tonight has not spoken

380
00:26:29,000 --> 00:26:32,000
of programs, neither Manolo nor I.

381
00:26:32,000 --> 00:26:35,000
We have spoken, and I confess it,

382
00:26:35,000 --> 00:26:38,000
of what moves us, him, me,

383
00:26:38,000 --> 00:26:41,000
José and the other colleagues.

384
00:26:41,000 --> 00:26:44,000
I do not know what will happen in the next months

385
00:26:44,000 --> 00:26:47,000
or in the next years,

386
00:26:47,000 --> 00:26:50,000
but the decision to maintain this speech is firm on our part.

387
00:26:50,000 --> 00:26:53,000
We will continue to maintain it,

388
00:26:53,000 --> 00:27:17,000
we will not change it.

