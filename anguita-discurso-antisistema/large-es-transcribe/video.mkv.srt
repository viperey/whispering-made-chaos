1
00:00:00,000 --> 00:00:22,360
Buenas noches. Hubo un hombre llamado Galileo Galilei, dedicado al estudio, a horas encerrado,

2
00:00:22,360 --> 00:00:31,240
viendo astros, sacando las conclusiones de su observación, que descubrió que la tierra

3
00:00:31,240 --> 00:00:37,960
no estaba en el centro del universo, que se movía, y por tanto era el sol el que ocupaba

4
00:00:37,960 --> 00:00:44,400
el centro y entorno al cual los planetas y entre ellos la tierra giraban. Aquel descubrimiento

5
00:00:44,400 --> 00:00:54,120
se enfrentó a la verdad institucionalizada. El Vaticano, la Iglesia, las creencias populares

6
00:00:54,120 --> 00:01:00,360
del momento, y la insistencia en el mantenimiento de lo que él había descubierto, le costó

7
00:01:00,360 --> 00:01:07,840
ir a juicio. Y frente al acusado, ¿cómo podía él pensar que se había equivocado

8
00:01:07,840 --> 00:01:15,440
a Aristóteles? ¿Cómo podía pensar él que las Sagradas Escrituras mentían? ¿Cómo

9
00:01:15,440 --> 00:01:20,440
podía atreverse a él, un ingenuo sabio, a pensar que haya descubierto algo que fuese

10
00:01:20,440 --> 00:01:26,360
en contra de lo que el magisterio de la Santa Madre Iglesia viene diciendo hacia siglos?

11
00:01:26,360 --> 00:01:31,720
Y sobre todo, ¿es que acaso el pueblo no aclamaba contra aquel que se atrevía a poner

12
00:01:31,720 --> 00:01:39,560
en duda la centralidad del planeta tierra? Las presiones son tremendas. Tiene casi que

13
00:01:39,560 --> 00:01:47,520
abjurar. Pero en un momento, en la rebeldía última, inmusitando casi con una sonrisa

14
00:01:47,520 --> 00:01:56,640
a los aramago, suave pero firme, dice en el italiano natal, E pur si muove, y sin embargo

15
00:01:56,640 --> 00:02:05,080
se mueve. Porque los cálculos matemáticos, porque las observaciones, porque el ejercicio

16
00:02:05,080 --> 00:02:11,920
de la razón, porque lo que sus ojos estaban viendo noche tras noche, estaban demostrando

17
00:02:11,920 --> 00:02:20,360
que era la tierra la que se movía. Pues bien, estamos hoy en la España de 1999,

18
00:02:20,360 --> 00:02:27,840
en la Europa de 1999 y en el mundo, en un momento en el que en otras ocasiones de la

19
00:02:27,840 --> 00:02:36,760
historia las sociedades han tenido que escoger un camino u otro. O seguir en la resignación

20
00:02:36,760 --> 00:02:46,280
o plantar cara la rebeldía que acaba de decir Manolo Cañada. La resignación es un producto

21
00:02:46,280 --> 00:02:54,620
que como cualquier droga duerme a la gente, duerme su conciencia. La resignación es como

22
00:02:54,620 --> 00:03:02,160
la morfina, la cocaína o la heroína. La resignación es producto de muchas causas.

23
00:03:02,160 --> 00:03:10,080
Yo voy a enumerar unas cuantas. La resignación es hija de ese discurso totalizador, cual

24
00:03:10,080 --> 00:03:17,360
si fuese una nueva religión. No hay más verdad que la competitividad. No hay más santos

25
00:03:17,360 --> 00:03:23,760
ni más poderes que los mercados. La economía tiene que crecer constantemente, no importa

26
00:03:23,760 --> 00:03:31,840
que se contaminen las aguas, que se contaminen los ríos, los mares o los aires. Competitividad,

27
00:03:31,840 --> 00:03:38,240
crecimiento sostenido y los mercados. Eso es lo único que importa. Su poder no puede

28
00:03:38,240 --> 00:03:45,360
ser contestado y además nos demuestra la existencia de las propias sociedades que esto

29
00:03:45,360 --> 00:03:52,200
es lo que produce bienestar. Y no importa que las personas de la calle vean que ese bienestar

30
00:03:52,200 --> 00:03:57,600
no le ha llegado al hijo o a la hija, que tiene que ir a la empresa de trabajo temporal

31
00:03:57,600 --> 00:04:04,720
que le cobra el 40% de la nómina por colocarlo en una empresa. No importa que la persona

32
00:04:04,720 --> 00:04:09,160
que todavía tiene una pensión, que no llega al salario mínimo interprofesional y está

33
00:04:09,160 --> 00:04:16,180
casi a la mitad, 69 mil y pico de pesetas, a veces no llega. No importa el paro de aquel

34
00:04:16,180 --> 00:04:22,160
que entró en los 45 años. No importa que la mujer madre y esposa, pero que además

35
00:04:22,160 --> 00:04:27,760
tiene que trabajar, no cobra lo mismo igual que el hombre haciendo la misma tarea, violando

36
00:04:27,760 --> 00:04:33,080
artículos enteros de la Carta Fundacional de las Naciones Unidas y la Declaración Universal

37
00:04:33,080 --> 00:04:38,960
de Derechos Humanos y texto de la Constitución Española. No importa, porque le están diciendo

38
00:04:38,960 --> 00:04:44,840
que no hay más bien que la competitividad, lo bien que vivimos, lo bien que vamos, los

39
00:04:44,840 --> 00:04:51,760
datos, las cifras. No importa que la gente vea o quiera ver en su entorno y a su alrededor

40
00:04:51,760 --> 00:04:58,440
hechos que están contradiciendo ese mensaje, porque para que no se vea o para que sea menos

41
00:04:58,440 --> 00:05:07,800
hiriente hay sucedáneos. Ahí tenéis la televisión, fútbol, mucho fútbol, más fútbol que en

42
00:05:07,800 --> 00:05:14,840
épocas anteriores de la historia de España. Ahí tenéis concursos degradantes que no

43
00:05:14,840 --> 00:05:22,980
alimentan la razón, el estudio, el análisis. Ahí tenéis la vida de los personajes populares

44
00:05:22,980 --> 00:05:29,160
que se diseccionan y se abren para que atidvemos como si fuéramos aves carroñeras y olvidando

45
00:05:29,160 --> 00:05:34,900
el entorno que tenemos entremos en lo que ocurre en sus alcobas. Ahí está toda una

46
00:05:34,900 --> 00:05:41,720
literatura de evasión para que la gente no vea. No vea y por tanto confunda su existencia

47
00:05:41,720 --> 00:05:48,680
real con la existencia que le ponen en las pantallas o en los informativos para que ocurra

48
00:05:48,680 --> 00:05:53,600
como aquello que tantas veces digo de la viejecita que a finales del siglo XIX estaba vendiendo

49
00:05:53,600 --> 00:05:58,960
cerillas a la puerta del palacio de la ópera de Madrid en un mes de enero a las dos de

50
00:05:58,960 --> 00:06:02,920
la madrugada aterida de frío y envuelta en una toquilla vendiendo cerillas para poder

51
00:06:02,920 --> 00:06:07,920
subsistir y cuando entraban hombres y mujeres envueltos en armiños, en capas, con lujo

52
00:06:07,920 --> 00:06:15,480
y con joya, decía qué bien vivimos en Madrid. Un caso de alienación, un caso de suplantación,

53
00:06:15,480 --> 00:06:23,400
un caso de drogadicción. La imagen, lo bien que vivimos, las historias de la alcoba, las

54
00:06:23,400 --> 00:06:28,880
revistas del corazón, las frivolidades que hacen olvidar lo que ocurre diariamente o

55
00:06:28,880 --> 00:06:36,520
si se ve se eleva a otra categoría como si no fuese lo real. Resignación además porque

56
00:06:36,520 --> 00:06:43,920
el discurso oficial que baja desde muchos sitios, baja desde los poderes públicos,

57
00:06:43,920 --> 00:06:49,400
baja desde las sentencias de los tribunales, desde las cátedras, desde las clases de EGB

58
00:06:49,400 --> 00:06:55,200
donde el maestro escuela va inyectando ya unas determinadas ideas, baja desde la televisión

59
00:06:55,200 --> 00:07:00,240
y de los medios de comunicación, el discurso de que no hay otra salida, esto es lo único

60
00:07:00,240 --> 00:07:05,800
posible. Si no, fijaros, estamos mal, pero peor estaban en el muro de Berlín y cuando

61
00:07:05,800 --> 00:07:10,160
ya se acude a hablar del muro de Berlín es porque no se tienen razones y hay que decir

62
00:07:10,160 --> 00:07:17,400
que mal fueron aquellos porque es la única justificación. Resignación porque los pueblos

63
00:07:17,400 --> 00:07:25,480
cuando tienen problemas no son rebeldes. El que tiene que comer todos los días no puede

64
00:07:25,480 --> 00:07:31,000
permitirse el lujo de perder por un acto de rebeldía el puesto de trabajo. La rebeldía

65
00:07:31,000 --> 00:07:37,480
siempre ha surgido de aquellos que comían todos los días. De aquí la gran culpabilidad

66
00:07:37,480 --> 00:07:42,840
de muchos intelectuales españoles que comiendo todos los días bien del pesebre o bien de

67
00:07:42,840 --> 00:07:57,200
su trabajo no han sido capaces de decir basta a esta situación de degradación. Una resignación

68
00:07:57,200 --> 00:08:04,240
que nace de la evidencia diaria del paro que es cierto, de ese paro que dicen que se reduce

69
00:08:04,240 --> 00:08:08,760
porque la estadística dice que cuando una persona trabaja dos horas a la semana ya no

70
00:08:08,760 --> 00:08:14,800
está parado. Una disminución estadística de los empleos a tiempo parcial, de las horas

71
00:08:14,800 --> 00:08:19,400
extraordinarias que se imponen pero que no se cobran, de la angustia si mañana poder

72
00:08:19,400 --> 00:08:25,880
trabajar. Eso es resignación. Resignación que cae sobre un pueblo que se da cuenta además

73
00:08:25,880 --> 00:08:30,880
o no se da cuenta porque no le gusta o no quiere verlo o no dejan verlo que estamos

74
00:08:30,880 --> 00:08:36,720
yendo hacia atrás, que estamos llegando a cotas propias del siglo XIX, que aquella seguridad

75
00:08:36,720 --> 00:08:41,720
social para todos, que el tema del subsidio de desempleo va bajando continuamente en contra

76
00:08:41,720 --> 00:08:47,120
de la declaración del Universal de los Derechos Humanos o de la propia Constitución. Resignación

77
00:08:47,120 --> 00:08:55,720
que surge de la culpabilidad del propio parado. Uno de los éxitos entre comillas del sistema

78
00:08:55,720 --> 00:09:05,040
americano es conseguir que el pobre, que el miserable se sienta culpable de su situación.

79
00:09:05,040 --> 00:09:12,280
Es la filosofía calvinista, hija del protestantismo. Tú eres culpable de tu situación. No has

80
00:09:12,280 --> 00:09:17,360
sido capaz de triunfar. Esa es la filosofía de la sociedad americana. Y si no has triunfado

81
00:09:17,360 --> 00:09:22,400
es porque tú eres el responsable. Esta sociedad da oportunidades a todo el mundo. Si tú no

82
00:09:22,400 --> 00:09:27,640
has podido hacerlo así, tú eres el culpable. Y entonces el oprimido, el pobrecito, el esclavo

83
00:09:27,640 --> 00:09:34,120
se echa en la responsabilidad de su situación. Es perfecto el dominio del poder. Un dominio

84
00:09:34,120 --> 00:09:38,760
del poder que ya no se basa en la fuerza, en la coacción, en la utilización de la

85
00:09:38,760 --> 00:09:44,080
Guardia Civil o del ejército. Se basa en un dominio mucho más terrible, más duro,

86
00:09:44,080 --> 00:09:50,320
el dominio de la mente. Ese opio que cae desde los aparatos de televisor, ese opio que cae

87
00:09:50,320 --> 00:09:54,600
desde la sentencia de los tribunales, desde los discursos políticos que va empapando

88
00:09:54,600 --> 00:09:59,800
la mentalidad de la gente y va diciendo calla, calla, calla, porque si no callas puede ser

89
00:09:59,800 --> 00:10:06,360
peor. Esa es la resignación que se produce como consecuencia de sentirse ese parado que

90
00:10:06,360 --> 00:10:11,560
él es el autor de su situación y por tanto aquel compañero que ha sido acusado de que

91
00:10:11,560 --> 00:10:17,600
cobró una vez indebidamente el seguro de desempleo, a mí se hable, tú eres el culpable.

92
00:10:17,600 --> 00:10:22,840
No importa que los ladrones de Alto Copete sean exhibidos como figuras brillantes a enseñarle

93
00:10:22,840 --> 00:10:27,520
a los hijos como ejemplo a seguir, pero el miserable que ha estofado solamente un mes

94
00:10:27,520 --> 00:10:33,600
del seguro de desempleo es el culpable de todo lo que está ocurriendo. Eso es resignación.

95
00:10:33,600 --> 00:10:39,160
Resignación que surge de los medios de comunicación y no se me enfaden las cámaras, no va con

96
00:10:39,160 --> 00:10:43,960
vosotros, pero va con los que tienen el poder en vuestras empresas, va contra aquellos que

97
00:10:43,960 --> 00:10:50,000
optan por decirle al pueblo una parte de la verdad. Resignación que consiste en dar un

98
00:10:50,000 --> 00:10:56,520
credo único, decir todos amén a la competitividad, a la moneda única, estamos mejor que nunca,

99
00:10:56,520 --> 00:11:02,120
amén, amén, amén, es el coro como en la letanía que va deformando el pensamiento,

100
00:11:02,120 --> 00:11:06,960
que va haciendo seres totalmente iguales como describía lo que podría ser el futuro Orwell

101
00:11:06,960 --> 00:11:16,540
en 1984. Esa resignación por tanto es hija de una economía, de un sistema político que

102
00:11:16,540 --> 00:11:23,440
confunde muchas cosas, una información que está haciendo surgir en nuestros universitarios,

103
00:11:23,440 --> 00:11:28,960
en nuestros institutos, en nuestras academias, en las escuelas básicas, la cultura del sí

104
00:11:28,960 --> 00:11:36,120
o no, propia del ordenador. La vida está llena de colores, de tonos y por tanto el

105
00:11:36,120 --> 00:11:41,680
lenguaje es mucho más vivo, cuanto hay más cosas que hay que ser descrita, sí o no,

106
00:11:41,680 --> 00:11:48,360
blanco o negro, derecha o izquierda, conteste usted como el ordenador, afirmativo, negativo,

107
00:11:48,360 --> 00:11:54,480
afirmativo, negativo, se busca ya no al ser humano pensante capaz de la reflexión, de

108
00:11:54,480 --> 00:12:00,320
la duda o de la inquietud, se busca al esclavo sin pensamiento. Y por eso no se quiere la

109
00:12:00,320 --> 00:12:06,440
historia y por eso se desdeña la memoria, porque los seres humanos somos hijos de la

110
00:12:06,440 --> 00:12:10,360
memoria, yo soy lo que soy porque viví con mis padres, mis recuerdos, mi historia, mi

111
00:12:10,360 --> 00:12:15,480
vivencia, yo soy la actualización de todo un pasado que está vivo, si me quitan la

112
00:12:15,480 --> 00:12:21,160
memoria soy un zombi, un muerto viviente y queremos pueblos de muertos vivientes que

113
00:12:21,160 --> 00:12:25,400
se estimulen por el último partido del mar Sabadrí, que se estimulen por la última

114
00:12:25,400 --> 00:12:30,280
historia del tal o cual conde o de tal o cual señora, que llegan en los corrillos incluso

115
00:12:30,280 --> 00:12:34,640
en los parlamentos y en los lugares donde había que debatirse de los problemas se cuenten

116
00:12:34,640 --> 00:12:40,240
chistes de la vida privada para olvidar la tremenda realidad, escapismo, droga, igual

117
00:12:40,240 --> 00:12:46,160
que la heroína, igual que la cocaína, droga, escapismo, cedar el pensamiento, aniquilar

118
00:12:46,160 --> 00:12:54,520
el espíritu crítico y por tanto fomentar la resignación y frivolidad, mucha frivolidad

119
00:12:54,520 --> 00:13:00,320
y por tanto la política entendida como compraventa de votos, no importa, que es lo que quiere

120
00:13:00,320 --> 00:13:05,600
el pueblo, al pueblo al cual convenientemente se le va a decir lo que quiere a través de

121
00:13:05,600 --> 00:13:09,720
determinados medios, más fútbol pues más fútbol, pero es que yo pienso que no, es

122
00:13:09,720 --> 00:13:14,960
que tú tienes que decir lo que le gusta al pueblo, al cual yo mediante medios de comunicación

123
00:13:14,960 --> 00:13:20,120
finísimo le voy diciendo que es lo que le conviene, pero yo represento un proyecto,

124
00:13:20,120 --> 00:13:24,360
yo quiero explicar un proyecto, yo quiero dirigir a mi pueblo de cual forma parte para

125
00:13:24,360 --> 00:13:29,160
decirle el punto de vista de nuestra organización, no, no, no, lo que conviene es que ganes votos,

126
00:13:29,160 --> 00:13:34,480
eso no está bien dicho, tienes que ser respetable, tienes que hablar y decirlo políticamente

127
00:13:34,480 --> 00:13:39,440
correcto, el buen tono, como el chico de la burguesía del siglo XIX, niño, eso no se

128
00:13:39,440 --> 00:13:44,120
hace, eso no se dice, tú lo haces por bajo cuerda, porque todo debe permanecer como si

129
00:13:44,120 --> 00:13:50,080
aquí no ocurriera nada, es decir la cultura de la hipocresía, crear una sociedad hipócrita

130
00:13:50,080 --> 00:13:55,220
que miente a sabienda, que sabe que está diciendo algo que nadie cree, pero lo importante

131
00:13:55,220 --> 00:14:01,380
no es decirlo, lo importante es que hay que hacerlo, pero que no se diga, y ese cáncer

132
00:14:01,380 --> 00:14:10,920
va avanzando, degradando, corrompiendo y aniquilando las fuerzas para combatir, y ese es un camino

133
00:14:10,920 --> 00:14:19,740
sin duda dulce, es la muerte lenta, como se consume un brasero, como van muriendo aquellos

134
00:14:19,740 --> 00:14:25,540
que beben la cicuta, muerte que le dieron al gran Sócrates, va durmiendo parotinamente

135
00:14:25,540 --> 00:14:33,320
todo el organismo y se muere uno con la sonrisa en los labios, pero muere, y el otro camino

136
00:14:33,320 --> 00:14:40,320
es lo que ha dicho Manolo, rebeldía, pero la rebeldía no es un gesto altisonante, no

137
00:14:40,320 --> 00:14:47,960
es un grito, no es un insulto, no es una pedrada, no es una mala contestación, es mucho más

138
00:14:47,960 --> 00:14:53,600
profundo, la rebeldía es un grito de la inteligencia y de la voluntad que dice, y lo voy a decir

139
00:14:53,600 --> 00:14:59,040
el román paladino, no me da la gana de decirle que sí a esta actual situación, ¿por qué?,

140
00:14:59,040 --> 00:15:04,240
porque no quiero y me niego a decirle que sí, porque entiendo que puede haber otra situación

141
00:15:04,240 --> 00:15:10,600
y por tanto yo no asumo esta condenumbre y no participo de ella y lucho contra ella,

142
00:15:10,600 --> 00:15:15,480
y esta actitud es una actitud intelectual, y cuando digo intelectual no quiero hablar

143
00:15:15,480 --> 00:15:21,000
de universitario, de la mente de cualquier ser humano, es un posicionamiento que nace

144
00:15:21,000 --> 00:15:26,640
de la mente y del corazón, del fuego, el querer cambiar, esta es la rebeldía fundamental,

145
00:15:26,640 --> 00:15:32,560
los otros son voces, son chillidos, son insultos, son granidos, dale caña, circo romano, no,

146
00:15:32,560 --> 00:15:37,160
la rebeldía no es ni más ni menos que el posicionamiento con otros valores y la decisión

147
00:15:37,160 --> 00:15:43,640
de hacer de frente, rebeldía para decir que no aceptamos que la competitividad y el mercado

148
00:15:43,640 --> 00:15:49,000
sean los que rijan los destinos de las sociedades, que entendemos que hay una declaración universal

149
00:15:49,000 --> 00:15:54,800
de derechos humanos que tiene que cumplirse y que eso significa sociedad de pleno empleo,

150
00:15:54,800 --> 00:16:00,000
donde el hombre y la mujer sean exactamente iguales, donde no haya marginados y que costará

151
00:16:00,000 --> 00:16:05,880
mucho tiempo y mucho sacrificio, pero es hermoso luchar, incluso morir por eso, porque morir

152
00:16:05,880 --> 00:16:11,520
tenemos que morir, duramos por lo menos luchando por un ideal noble y no consumiéndonos como

153
00:16:11,520 --> 00:16:23,480
un brasero y significa esa rebeldía fundacional en cuanto a entidad humana, significa defender

154
00:16:23,480 --> 00:16:28,920
con esa suave ironía, con esa tranquilidad que el maestro Saramago hace, porque es una

155
00:16:28,920 --> 00:16:34,400
gloria verlo contestar a los periodistas con esa suave ironía, con esa tremenda dureza

156
00:16:34,400 --> 00:16:42,440
de fondo, pero flexibilidad en el lenguaje, significa defender que hay valores que deben

157
00:16:42,440 --> 00:16:50,120
ser mantenidos, el hermoso valor de la igualdad, como decía uno, la sangre es roja y todos

158
00:16:50,120 --> 00:16:55,560
la tenemos roja, no hay sangre azul y además, como decía otro, todos los corazones, salvo

159
00:16:55,560 --> 00:17:04,400
una sesión, están en la izquierda, por tanto esa igualdad, igualdad que hace que los seres

160
00:17:04,400 --> 00:17:10,960
humanos nazcan de la misma manera, una igualdad esencial, no igualitarismo y por tanto dignidad

161
00:17:10,960 --> 00:17:18,680
de la persona por ser lo que es, persona y junto a la igualdad la libertad, pero hablar

162
00:17:18,680 --> 00:17:24,640
de libertad es algo muy grande, porque libertad es asumir que se tiene la conciencia libre

163
00:17:24,640 --> 00:17:30,280
que no es lo mismo que libertad de conciencia, la conciencia libre significa que yo puedo

164
00:17:30,280 --> 00:17:37,080
decidir si yo tengo todos los elementos para formular mi decisión, estoy bien informado,

165
00:17:37,080 --> 00:17:42,960
estoy bien formado, me alimento todos los días, tengo un techo donde guarecerme, tengo

166
00:17:42,960 --> 00:17:48,240
una ropa que ponerme y una vez que tengo mis necesidades más elementales satisfechas,

167
00:17:48,240 --> 00:17:53,500
yo puedo empezar a pensar para ser un hombre libre, porque si yo tengo que buscar el trabajo

168
00:17:53,500 --> 00:17:58,800
trampeando como sea, poniéndome en la cola del paro, vendiéndome por cuatro perras porque

169
00:17:58,800 --> 00:18:03,380
tengo que comer los míos y yo, yo no soy hombre libre aunque mañana me permitan que

170
00:18:03,380 --> 00:18:10,080
vaya a votar en las urnas, yo voy movido por mi hambre, por mi necesidad de tener que venderme

171
00:18:10,080 --> 00:18:19,640
en cada momento para el trabajo y junto a la libertad en sentido espléndido de la palabra,

172
00:18:19,640 --> 00:18:25,760
la justicia y no hablo de tribunales de justicia, hablo de eso tan sencillo de dar a cada uno

173
00:18:25,760 --> 00:18:32,000
lo suyo, que impere el derecho, que no haya distinciones, que todo el mundo sea medido

174
00:18:32,000 --> 00:18:37,560
por igual rasero, por el rasero de la ley, la justicia que consiste además en que se

175
00:18:37,560 --> 00:18:43,460
conforma una sociedad, la ley es la que puede hacer posible que conviva la gente en sociedad

176
00:18:43,460 --> 00:18:49,760
mientras que la ley sea justa y se aplique con justicia a todos igual, solidaridad, es

177
00:18:49,760 --> 00:18:55,140
un mensaje que nos puede hermanar a todos, a todos aquellos que hablaban del internacionalismo

178
00:18:55,140 --> 00:19:02,000
proletario que sigue estando vigente, a aquellos que hablan de la hermandad de los seres humanos

179
00:19:02,000 --> 00:19:06,840
y porque hacen referencia a sus creencias basadas en la teología de liberación, a

180
00:19:06,840 --> 00:19:11,320
otros que hablan desde otros supuestos de liberación humana, a otras propuestas de

181
00:19:11,320 --> 00:19:17,160
liberación, de acuerdo, solidaridad que consiste en afirmar tranquila y serenamente que no

182
00:19:17,160 --> 00:19:21,640
merece la pena luchar por bandera, que la única bandera es la bonera del planeta tierra

183
00:19:21,640 --> 00:19:26,500
y la humanidad es una sola raza, una sola y única raza y que merece la pena luchar

184
00:19:26,500 --> 00:19:33,320
por ella. Y esto es muy importante, informado, no porque se le den muchas noticias, hay diferencia

185
00:19:33,320 --> 00:19:37,880
entre la noticia de información, la noticia es una mercancía que se da para que se consuma,

186
00:19:37,880 --> 00:19:42,240
la información es un dato que se da para que la gente piense y a partir de ahí extraiga

187
00:19:42,240 --> 00:19:49,840
sus consecuencias. Y desde la izquierda, hablar de austeridad, a mí particularmente me gusta

188
00:19:49,840 --> 00:19:56,520
esta palabra, la austeridad, hablar de austeridad fue la palabra que vertebró un discurso del

189
00:19:56,520 --> 00:20:00,960
Enrico Berlinguer, aquel secretario general del partido comunista italiano que murió

190
00:20:00,960 --> 00:20:09,120
en la tribuna, hablando precisamente de austeridad, la austeridad en el sentido romano, mediterráneo,

191
00:20:09,120 --> 00:20:15,240
austeridad no es miseria, austeridad significa vivir dignamente, normalmente, no malgastar

192
00:20:15,240 --> 00:20:22,000
los recursos naturales, poseer uno cosas y no que las cosas lo posean a uno, no ir constantemente

193
00:20:22,000 --> 00:20:27,920
atentando con la naturaleza en un consumismo feroz, austeridad significa tiempo libre para

194
00:20:27,920 --> 00:20:33,280
discutir y dialogar con los demás, para jugar, para hacer posible el amor entre seres que

195
00:20:33,280 --> 00:20:38,120
se conocen, para convivir en la calle, en la plaza, en el árbol agriega, austeridad

196
00:20:38,120 --> 00:20:42,560
que significa que la mejor manera de vivir es tener relaciones con otros en el plano

197
00:20:42,560 --> 00:20:47,840
de igualdad, sintiéndose hombres y mujeres libres en una sociedad democrática, austeridad

198
00:20:47,840 --> 00:20:52,860
que hace que nos miran a todos como seres humanos y no por nuestra capacidad de consumo,

199
00:20:52,860 --> 00:20:57,040
yo me niego como ser humano que digan que soy un español que consume tanta salchicha

200
00:20:57,040 --> 00:21:03,080
o tontos coches al año, eso no es la austeridad, eso es medir al ser humano por otro talante,

201
00:21:03,080 --> 00:21:09,280
austeridad que significa con otra palabra sobriedad, hablar de cosas concretas, hablar

202
00:21:09,280 --> 00:21:15,760
de cosas que son importantes e incluso cuando se utiliza el lenguaje para crear belleza,

203
00:21:15,760 --> 00:21:20,960
para hacer pensar como nuestro premio Nobel, se utiliza el lenguaje desde la sobriedad

204
00:21:20,960 --> 00:21:27,920
porque las palabras cayendo en cascada, uniéndose, recreándose constantemente, hacen pensar,

205
00:21:27,920 --> 00:21:33,840
hacen concebir nuevas ideas, humanizan, esa es la austeridad y esa es la sobriedad y a

206
00:21:33,840 --> 00:21:39,720
partir de ahí es cuando comienza el discurso y la propuesta, la sociedad de plenumpleo,

207
00:21:39,720 --> 00:21:45,560
el desarrollo sostenible, el reparto del trabajo, es decir el discurso rojo, verde, violeta,

208
00:21:45,560 --> 00:21:51,280
el discurso de la paz, paz y la paz no es la ausencia de guerra, la paz por ejemplo

209
00:21:51,280 --> 00:21:56,800
es que el día 9 estemos llenando roca porque quieren transformar la base militar en una

210
00:21:56,800 --> 00:22:02,160
super base violando el punto tercero de lo que acordó el pueblo español en el referéndum

211
00:22:02,160 --> 00:22:09,040
en 1986, la paz significa que mañana 1200 hombres y aviones españoles que cuestan un

212
00:22:09,040 --> 00:22:15,760
dinero no puedan entrar en la antigua Yugoslavia porque no han sido consultados las Cortes

213
00:22:15,760 --> 00:22:21,760
Generales y porque se ha violado nuevamente el artículo 62 de la Constitución, significa

214
00:22:21,760 --> 00:22:28,880
por tanto hablar de paz, paz como justicia, como entendimiento entre seres iguales que

215
00:22:28,880 --> 00:22:37,680
son capaces de razonar y bien los mecanismos son los de siempre, la movilización, ¿qué

216
00:22:37,680 --> 00:22:47,260
es movilizar? desde la izquierda siempre movilizar no ha sido solo llenar las calles de gente

217
00:22:47,260 --> 00:22:57,280
que también, movilizar ha sido concienciar, nosotros existimos los que queremos pensar

218
00:22:57,280 --> 00:23:05,760
por nuestra cuenta para perturbar a los demás, si hay aquí algún creyente me dirijo a él

219
00:23:05,760 --> 00:23:12,400
o a ella, para recordarle la frase que hoy explicaba yo en la universidad cuando una

220
00:23:12,400 --> 00:23:16,520
persona un compañero que era representante parece de la teología de la liberación

221
00:23:16,520 --> 00:23:22,440
me preguntaba y le recordaba yo un pasaje del evangelio de mi época pasada soy conocedor

222
00:23:22,440 --> 00:23:26,880
decía mirad una de las cosas que figuran el evangelio es cuando le preguntan a Jesús

223
00:23:26,880 --> 00:23:32,880
de Galilea ¿tú que has venido aquí a traer la paz? dice yo no, he venido a traer la guerra

224
00:23:32,880 --> 00:23:38,400
y ¿qué quería decir? he venido a concienciar a perturbar, nosotros no queremos gente tranquila

225
00:23:38,400 --> 00:23:45,660
drogada queremos gente que inquieta, venimos a perturbar a agitar cerebro a mover conciencia

226
00:23:45,660 --> 00:23:50,800
existimos en la medida que movilicemos el pensamiento como decía en aquella iglesia

227
00:23:50,800 --> 00:23:55,560
que el barrio naranjo de Córdoba levántate y piensa en lo más revolucionario que he

228
00:23:55,560 --> 00:24:00,880
visto en mi vida porque la rebeldía empieza aquí en la cabeza que dice no sirvo no me

229
00:24:00,880 --> 00:24:08,440
da la gana no quiero asumir estos valores movilización que significa por tanto ese

230
00:24:08,440 --> 00:24:14,800
esfuerzo por pensar y por hacer pensar los grandes revolucionarios de la historia la

231
00:24:14,800 --> 00:24:20,080
característica fundamental fue que hicieron pensar la revolución la hicieron las gentes

232
00:24:20,080 --> 00:24:26,300
las masas los colectivos pero el valor de ellos es el pensamiento que pusieron en marcha

233
00:24:26,300 --> 00:24:32,140
es el concepto de la movilización en torno a lo concreto y con las alianzas de todo el

234
00:24:32,140 --> 00:24:39,840
pueblo por eso hacemos llamamientos queremos unidad pero no para repartirse sillones para

235
00:24:39,840 --> 00:24:44,400
hacer programas de transformación ¿qué hacemos en el pueblo? ¿qué hacemos en la

236
00:24:44,400 --> 00:24:50,280
comunidad autónoma? ¿qué hacemos en España? ¿qué hacemos en Europa? alianzas, alianzas

237
00:24:50,280 --> 00:24:55,700
entre gentes que coinciden básicamente parece ser por lo menos teóricamente en que quiere

238
00:24:55,700 --> 00:25:00,360
cambiar el mundo pongámonos de acuerdo que podemos cambiar ahora pero cambiar un sillón

239
00:25:00,360 --> 00:25:06,140
por otro eso ya no es correcto eso lo hacen los otros desde tiempo inmemorial y por último

240
00:25:06,140 --> 00:25:15,440
la cultura la palabra cultura viene de cultivo cultivarse hacerse ser humano cada día más

241
00:25:15,440 --> 00:25:22,480
la cultura no es saber muchas cosas la cultura es captar todo aquello que la humanidad ha

242
00:25:22,480 --> 00:25:30,040
ido produciendo y que nos mueve desde el arte al estremecimiento por degustar la belleza

243
00:25:30,040 --> 00:25:36,560
a entender cómo la humanidad ha ido superando determinados problemas un hombre culto no

244
00:25:36,560 --> 00:25:41,640
es un hombre que esté rodeado de libros que también puede ser un hombre culto es un hombre

245
00:25:41,640 --> 00:25:47,460
que mira al mundo con mirada independiente y libre un hombre culto puede ser un campesino

246
00:25:47,460 --> 00:25:53,120
de nuestras tierras cuando rebina palabras que utilizan en mi tierra está pensando pero

247
00:25:53,120 --> 00:25:58,540
sabe calcular las cosas piensa como quiere es un hombre que tiene un tipo de cultura

248
00:25:58,540 --> 00:26:04,180
y ese hombre que a lo mejor no sabe leer le puede dar la mano a otro culto de la universidad

249
00:26:04,180 --> 00:26:09,000
que sabe más cosas pero está en la onda de la cultura porque ambos confíen desde

250
00:26:09,000 --> 00:26:17,780
su sentido de hombres libres con capacidad para pensar y en fin en el acto de hoy donde

251
00:26:17,780 --> 00:26:22,820
ahora va a tomar la palabra el maestro saramago y dicho con todo cariño en el sentido de

252
00:26:22,820 --> 00:26:28,880
ejercicio de sencillez y de untura la voz de izquierda unida esta noche no ha hablado

253
00:26:28,880 --> 00:26:36,520
de programas ni manolo ni yo hemos hablado y os lo confieso de lo que nos mueve a nosotros

254
00:26:36,520 --> 00:26:44,280
a él a mí a jose y a los demás compañeros y compañeras no sé lo que ocurrirá en los

255
00:26:44,280 --> 00:26:49,780
próximos meses o en los próximos años pero la decisión de mantener este discurso es

256
00:26:49,780 --> 00:27:07,220
firme por nuestra parte la vamos a seguir manteniendo no lo pensamos cambiar.

