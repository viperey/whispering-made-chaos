# Whispering made chaos

## TL;DR

Some examples of using Whisper automatic speech recognition for transcribing and translating diverse 
political videos.  
The output is a "good enough" result with 0 human edition of the text.  
  
This simple test case opens the door to consider this kind of technologies for increasing the 
capacity of different radical-left groups to spread their material and make it accesible world-wide 
with very little effort.

---

## Video selection

+ What's the video about?
+ What makes the video interesting for checking Whisper's capabilities?


### 7 inbraak methoden (7 methods of breaking in)

https://video.liberta.vip/w/tQyz93mbTEs9ZG1K2rugyi

A educational video made by the Dutch police where their aim was to help people prevent thieves from
entering their houses but instead they made an amazing tutorial for squatters on how-to break in
most of the locks houses have in The Netherlands.

The video has only one voice, based on a script, good audio quality. It should be an easy test case.


### Anguita: el gran discurso antisistema (Anguita and the anti-system speech)

https://video.liberta.vip/w/rhTuiNqDvvTZoeECJ3nKTN

Video from 1999 given by Julio Anguita (Communist Party leader in Spanish between 80s and 90s).

The vocabulary of the speech is complex, leaning academic and the speaker uses lenghty and 
subordinate sentences. Even at some moments the sentences are gramatically incorrect.


### El fascismo que viene (The upcoming fascism)

https://video.liberta.vip/w/x61JXuvxSZGZpac8q7yXyq

Documentary from 1994 about the 90s rise of neonazism across Europe.

There are multiple voices in the video, private expressions, many names from different languages.


### Entrevistas a García Oliver (García Oliver's interviews)

Part 1: https://video.liberta.vip/w/4o58SGH43tAXYqu2TzvXx4
Part 2: https://video.liberta.vip/w/9oSWGvzugJdNoiK7gAWAFL

Interview from 1977 to historical AnarchoSyndicalist leader García Oliver. The interview goes about
anarchism and the revolutionary events that took place across Spain during the Spanish Civil War.

The audio is in quite bad quality, sometimes is hard to understand what's Oliver saying. The 
vocabulary he uses is quite dense and old fashion (considering he left Spain and lived in Mexico).
He also uses political expressions from the 30s mixed with methaphorical ideas, so it's a hard case 
for translation.


### In een tank jij kan niet wonen (You cannot live in a tank)

https://video.liberta.vip/w/x8EYmnAh4uts3xtgposiYk

Documentary from 1980 about the squatting scene in Amsterdam.

It's a hard test case for transcription and translation since the Dutch language changes quite a lot 
over the years and this video is an example of it. Vocabulary and expressions can be old fashion
or outdated, the quality of the audio is neither the best, so it's interesting to see how does
Whisper deal with it.

Can be compared with: https://kolektiva.media/w/adCyZC4isHs5WQ55pqJqUk

### Precaristas (Precarious)

https://video.liberta.vip/w/84RfUHRMpEABxt2tVCLpLD

Documentary from 2018 about the housing struggle in the Canary Islands.

The audio quality is good, there are many voices, the mayority of them have a thick Canary Island 
accent which is interesting for checking the precision of Whisper over the most common accent in
Spanish: latin american ones.
This video was chosen to be compared with a manual subtitling made in the past. The result is higly
good, both transcription and translation.

Can be compared with: https://kolektiva.media/w/gpvBtkad91V7ex7DETjQT2

### Waar de ratten koning zijn (Where the rats are the kings)

https://video.liberta.vip/w/1cPNCHLg75ix1duo51Lq5x

Documentary from 1985 about the punk culture in The Netherlands in 1985.

It's a similar case to `In een tank jij kan niet wonen`.

---

## What's whisper

https://openai.com/blog/whisper/

It's a neural network specialied in transforming audio into text.  
Regardless of the spoken language.  

The code it's open source, although the organization behind it has business interests.


## How can whisper be used

The Whisper project has released a python tool that can be downloaded, installed and used 
autonomously for anyone interested.

### Setup

Install some basic stuff
```
sudo apt-get install ffmpeg
pip install setuptools-rust
pip install git+https://github.com/openai/whisper.git
```

### Run

Command example for transcription:
`whisper --model large --device cpu --language es --task transcribe --output_dir . video.mkv`

Command example for transcription:
`whisper --model large --device cpu --language es --task translate --output_dir . video.mkv`


## Benchmarks

Rending time is about 4 times the original video/audio length in a regular desktop PC when using
the large model.

The desktop is functional during the rendering, although the process takes up to 80% of the available
CPU time available.

---

# Conclusions

### The technical ones

This is just a test case made within minutes, 0 tweaking of the recognition options, leaving the 
quality of the results to the usage of the largest model available in exchange of time and resources.

The results are not homogeneous and the different videos have diverse strong and weaks areas.

In some cases names recognition was wrong, from instance in Garcia Oliver's video, when he's
speaking Spanish but naming people with Catalan surnames, the grammar is what one would except in
Spanish, but not in Catalan. It's a "logical" mistake, considering the lack of context when
performing the translation/transcription.

In general, the results are amazing and the little mistakes on grammar level can easily be solved 
with some human edition.

Also, something to consider. Whisper is not a captioning tool, as in, it does not generate 
subtitles.
It does generate a transcription of what's heard, but it does not organises the output in terms
of who's speaking or what other things are being heard, like music, laughter and so.

Let's not confuse both things, because whereas transcription would be helpful as a baseline for
sharing content, it does not reach the needed quality level for considering it ready for people
with hearing problems, for instance.

It should also be taken into account that this results are the baseline from now on.
In other words, technically, this will only get better over time and we can expect more and more
impressive leaps in the comming weeks and months until it reaches a level of maturity where it
becomes a de-facto standard and it's simply another tool we have available for usage.


### The political ones

The hardest part of this experiment.

I've dealt with too many different projects in the past that required translation as for knowing
how much time can be saved with these "AI" tools.

And personaly, I'd not leave it only to video, but also to text since equivalent tools are
available as open-source for AI based translation or text improval.

But, we should never lack critique when new technologies appear.
I believe most of the tecnical progress done and available for humans is positive in it's 
core nature, although, the way it's used, the power structures that control and/or enforce it 
make some tools become another brick in the opression wall rather than useful or liberating ones.

In any case, the biggest political challengue faced here is understanding who's the owner of this
technology and what happens when we used it for some time, we get a dependency of it and later on
its owner decides to close the code and monetize the app.

That's the reason why we should always lean to open-source projects.
However, when it comes to AI we face a new type of problem: we do not understand the code of the AI
because the AI are black boxes from which, at this moment, reverse engenieering is not doable.
In simpler words: we know it works, we kinda know how it works but we cannot re-write the AI from
0 and liberate a fully-open-source solution if needed.

So. We know the risks and we know history, so not much room for optimism.
But, it can also be argued, that even the smallest open-source AI projects have a good element in 
their favor: AI do only improve over time and it's pace accelerates over time.
Meaning: even if small grassroots projects do not produce the same quality of results as 
professional and well-funded options, over time, they/we will reach that point because it's a matter
of time.

As a personal note, when looking back at my experience doing "Precaristas" subtitles, where Youtube
subtitles were used, it took around 4 edition for the Spanish case and about 12 hours to fix and 
place correctly the English subtitles.
Currently with this tool, not much more than 1 and 2 hours would be needed to leave behing high 
quality subs.

