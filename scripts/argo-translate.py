import argostranslate.package
import argostranslate.translate

from_code = "es"
to_code = "en"

# Download and install Argos Translate package
available_packages = argostranslate.package.get_available_packages()
package_to_install = next(
    filter(
        lambda x: x.from_code == from_code and x.to_code == to_code, available_packages
    )
)
argostranslate.package.install_from_path(package_to_install.download())

# Translate

with open('large-es-transcribe/video.mkv.plain.txt') as f:
    contents = f.read()
    print(contents)
    translatedText = argostranslate.translate.translate(contents, from_code, to_code)
    print(translatedText)
# '¡Hola Mundo!'