1
00:00:00,000 --> 00:00:03,000
I'm standing here because I want to show that I'm behind Johannes.

2
00:00:03,000 --> 00:00:06,000
He has cracked and has been in arrest for weeks.

3
00:00:06,000 --> 00:00:10,000
And I want to show everyone that I'm behind Johannes.

4
00:00:10,000 --> 00:00:14,000
I have seen with great pleasure a number of years ago

5
00:00:14,000 --> 00:00:17,000
how that crack movement was very creative

6
00:00:17,000 --> 00:00:21,000
and developed with very good motivations

7
00:00:21,000 --> 00:00:25,000
and how they did very nice actions without any violence.

8
00:00:25,000 --> 00:00:28,000
I have also seen how violence was responded to

9
00:00:28,000 --> 00:00:30,000
and how there was an increasing escalation.

10
00:00:30,000 --> 00:00:34,000
It didn't start with the crackers, it started with the lawful authority.

11
00:00:34,000 --> 00:00:37,000
That the crackers now want to defend themselves against such attacks,

12
00:00:37,000 --> 00:00:39,000
I can well imagine.

13
00:00:39,000 --> 00:00:41,000
We are now peacefully demonstrating here.

14
00:00:41,000 --> 00:00:46,000
If they attack us here or whatever, I don't know what I'm doing.

15
00:00:46,000 --> 00:00:49,000
You can't let that happen to everyone, that's too crazy.

16
00:00:49,000 --> 00:00:53,000
Are we living in a police state or are we living in a justice state?

17
00:00:53,000 --> 00:00:58,000
I would like to see that from a distance.

18
00:01:23,000 --> 00:01:28,000
35 years of welfare state has not been able to end the housing crisis.

19
00:01:28,000 --> 00:01:32,000
Large groups of the population live in bad or too small houses.

20
00:01:32,000 --> 00:01:35,000
And although the government has recognized the right to housing for young people,

21
00:01:35,000 --> 00:01:38,000
they don't get along at all.

22
00:01:38,000 --> 00:01:41,000
There is no money, no land, but above all no political will

23
00:01:41,000 --> 00:01:44,000
to see housing as a priority.

24
00:01:44,000 --> 00:01:46,000
Cities are collapsing.

25
00:01:46,000 --> 00:01:49,000
There is no money, no land, but above all no political will

26
00:01:49,000 --> 00:01:52,000
to see housing as a priority.

27
00:01:52,000 --> 00:01:54,000
Cities are collapsing.

28
00:01:54,000 --> 00:01:57,000
Houses are being demolished.

29
00:01:57,000 --> 00:02:01,000
Speculators are making a lot of money on the scarce housing.

30
00:02:01,000 --> 00:02:08,000
Instead of affordable housing, expensive offices and luxury apartments are being built.

31
00:02:08,000 --> 00:02:20,000
Housing in need! Housing in need!

32
00:02:21,000 --> 00:02:24,000
Protests of the population and actions for a different housing policy

33
00:02:24,000 --> 00:02:28,000
are breaking on the non-promising promises of the authorities.

34
00:02:32,000 --> 00:02:36,000
The only thing that the Amsterdam People's Assembly has no shortage of at the moment

35
00:02:36,000 --> 00:02:38,000
is housing seekers.

36
00:02:38,000 --> 00:02:41,000
You can say that of every six Amsterdam households,

37
00:02:41,000 --> 00:02:44,000
at the moment one is looking for a home.

38
00:02:44,000 --> 00:02:49,000
I would say, in the hope that you will do something about it quickly and effectively,

39
00:02:49,000 --> 00:02:52,000
our request package and our letter in a row.

40
00:02:52,000 --> 00:02:54,000
Thank you.

41
00:02:56,000 --> 00:03:01,000
I don't think it's a question of whether you have to deal with these kinds of situations,

42
00:03:01,000 --> 00:03:03,000
it's a question of how you have to deal with them.

43
00:03:03,000 --> 00:03:06,000
We are ready to talk about it at any moment.

44
00:03:06,000 --> 00:03:09,000
I consider this part of the conversation.

45
00:03:09,000 --> 00:03:15,000
I think I can only conclude by saying that I hope that at that point,

46
00:03:15,000 --> 00:03:18,000
both in terms of the course of affairs in Amsterdam

47
00:03:18,000 --> 00:03:22,000
and in terms of our, I think, strong position in The Hague,

48
00:03:22,000 --> 00:03:27,000
I hope that the City Council of Amsterdam will be able to do that.

49
00:03:27,000 --> 00:03:30,000
Not against the groups that have offered me this piece,

50
00:03:30,000 --> 00:03:34,000
but alongside and together with the groups that have offered me this piece.

51
00:03:34,000 --> 00:03:36,000
Thank you.

52
00:03:36,000 --> 00:03:39,000
Does anyone have a piece of paper?

53
00:03:58,000 --> 00:04:02,000
The moment Paul opens the door, and that is clearly visible,

54
00:04:02,000 --> 00:04:07,000
he drives his car away and the barricade team,

55
00:04:07,000 --> 00:04:13,000
that's six people, they step outside and they walk to the building.

56
00:04:13,000 --> 00:04:17,000
The first cracks are made at the end of the 1960s.

57
00:04:17,000 --> 00:04:22,000
Since then, thousands of empty houses have been added to the housing stock.

58
00:04:22,000 --> 00:04:26,000
In the beginning, mainly tightly nailed walls are cracked.

59
00:04:26,000 --> 00:04:29,000
Due to the wrong approach of the city council,

60
00:04:29,000 --> 00:04:32,000
the building has been demolished.

61
00:04:32,000 --> 00:04:35,000
The building has been demolished,

62
00:04:35,000 --> 00:04:38,000
the building has been demolished,

63
00:04:38,000 --> 00:04:41,000
the building has been demolished,

64
00:04:41,000 --> 00:04:44,000
the housing stock has been needed.

65
00:04:44,000 --> 00:04:47,000
Due to the wrong approach of the city council,

66
00:04:47,000 --> 00:04:50,000
the building has been demolished.

67
00:04:50,000 --> 00:04:54,000
Slowly cracks have also focused on larger and more expensive buildings.

68
00:04:54,000 --> 00:04:58,000
These buildings lend themselves well to live in a group.

69
00:04:58,000 --> 00:05:03,000
Cracking offers the possibility to experiment with different ways of life.

70
00:05:03,000 --> 00:05:08,000
But whether you live in a group now or only, it remains uncertain.

71
00:05:08,000 --> 00:05:13,000
Although cracking is not punishable in itself, an owner can go to the court to have you cleared.

72
00:05:13,000 --> 00:05:16,000
He must therefore know your last name.

73
00:05:16,000 --> 00:05:20,000
Recently, judges have decided to clear you without knowing your last name.

74
00:05:20,000 --> 00:05:24,000
This puts them ahead of the new vacancy law.

75
00:05:24,000 --> 00:05:28,000
If this law is in effect, cracking will be punishable in most cases.

76
00:05:28,000 --> 00:05:34,000
But it wasn't that difficult when Margreet cracked a closed-off community home in De Pijp a few years ago.

77
00:05:34,000 --> 00:05:40,000
Well, the floor was completely sawed open from the room.

78
00:05:40,000 --> 00:05:44,000
The windows were nailed shut.

79
00:05:44,000 --> 00:05:49,000
In the alcove there was a stack of glass and a stack of wood.

80
00:05:49,000 --> 00:05:51,000
Those were the windows.

81
00:05:51,000 --> 00:05:55,000
Furthermore, the closet was quite damaged by the building management.

82
00:05:55,000 --> 00:05:57,000
No plug contacts.

83
00:05:57,000 --> 00:06:01,000
The electricity wires were pulled out into the wall.

84
00:06:01,000 --> 00:06:03,000
The toilet bowl was broken.

85
00:06:03,000 --> 00:06:05,000
The drainpipe was no longer there.

86
00:06:05,000 --> 00:06:08,000
So that was quite a bit of work.

87
00:06:08,000 --> 00:06:11,000
Are there many of these houses in the neighborhood?

88
00:06:11,000 --> 00:06:17,000
Well, especially here in the Gerredouw-Kolein neighborhood, there are quite a few houses that have been nailed shut and cracked.

89
00:06:17,000 --> 00:06:23,000
I have heard from people's personal reactions that they think that's fine.

90
00:06:23,000 --> 00:06:28,000
That they don't like to look at nailed-down buildings themselves.

91
00:06:28,000 --> 00:06:32,000
Are there still many of these cracked buildings in Amsterdam?

92
00:06:32,000 --> 00:06:38,000
No, because the building management has finally put an end to that policy.

93
00:06:38,000 --> 00:06:42,000
It took years before they found out that it was ridiculous.

94
00:06:42,000 --> 00:06:47,000
Well, that the first people who lived there had to be rehabilitated.

95
00:06:47,000 --> 00:06:50,000
Then the buildings were destroyed.

96
00:06:50,000 --> 00:06:52,000
One time worse than the other.

97
00:06:52,000 --> 00:07:01,000
And that people, because they had nothing else, or lived in a very small attic, were cracking and cracking.

98
00:07:01,000 --> 00:07:05,000
As quiet as Magreet lives, most of the cracks live in Amsterdam.

99
00:07:05,000 --> 00:07:09,000
But if you're unlucky, you'll get a group of thugs on your roof.

100
00:07:09,000 --> 00:07:38,000
By defending your house together, it is possible to scare off such a group of thugs.

101
00:07:38,000 --> 00:07:42,000
It's not just private landlords who send thugs.

102
00:07:42,000 --> 00:07:48,000
Landlord Schever decides at the end of 1978 to vacate a number of buildings in the Kinken neighborhood.

103
00:07:48,000 --> 00:07:53,000
According to building and housing management, there will be an acute risk of collapse.

104
00:07:53,000 --> 00:08:09,000
The architect of the neighborhood comes to a completely different conclusion.

105
00:08:09,000 --> 00:08:15,000
The municipal administration refuses to talk and sends a mobile unit.

106
00:08:15,000 --> 00:08:21,000
The cracks that form in violent chains around the buildings are broken up.

107
00:08:21,000 --> 00:08:25,000
The last bit of trust in the municipal administration has disappeared.

108
00:08:25,000 --> 00:08:54,000
Amsterdam, Amsterdam, Amsterdam!

109
00:08:54,000 --> 00:09:01,000
The fear of even more evacuations and a threatening anti-crack law make the cracks better organize themselves.

110
00:09:01,000 --> 00:09:05,000
In many neighborhoods there are crack parlors and crack cafes.

111
00:09:05,000 --> 00:09:11,000
These cafes are becoming more and more important as a meeting place where actions are also being prepared.

112
00:09:11,000 --> 00:09:26,000
Through the telephone alarm list, cracks can be called up quickly.

113
00:09:26,000 --> 00:09:46,000
The crack office delivers information that is less than necessary.

114
00:09:46,000 --> 00:09:58,000
What do you think of the cracks?

115
00:09:58,000 --> 00:10:01,000
I don't know what's going on under the cracks.

116
00:10:01,000 --> 00:10:03,000
The people who are in the big cracks?

117
00:10:03,000 --> 00:10:04,000
They don't get cracks.

118
00:10:04,000 --> 00:10:08,000
Those people also have a need for a house under their bed.

119
00:10:08,000 --> 00:10:11,000
They are going to cry or they are going to live in a cave.

120
00:10:11,000 --> 00:10:14,000
Or sleep under the bridge and hang a couple of belts.

121
00:10:14,000 --> 00:10:16,000
They have the right to do that.

122
00:10:16,000 --> 00:10:18,000
All those offices that are empty.

123
00:10:18,000 --> 00:10:20,000
In the hands of the state.

124
00:10:20,000 --> 00:10:22,000
But they do so little.

125
00:10:22,000 --> 00:10:26,000
They do so little for that class.

126
00:10:26,000 --> 00:10:30,000
Through a better organization, it becomes possible to take large speculation buildings.

127
00:10:30,000 --> 00:10:34,000
Six empty buildings of the Ooghem on the Kijsersgracht are cracked.

128
00:10:34,000 --> 00:10:37,000
And become known under the name of the Great Emperor.

129
00:10:37,000 --> 00:10:40,000
Rob is one of the first residents.

130
00:10:40,000 --> 00:10:42,000
Why did I crack?

131
00:10:42,000 --> 00:10:44,000
It was a kind of development.

132
00:10:44,000 --> 00:10:46,000
At some point my parents moved.

133
00:10:46,000 --> 00:10:49,000
I lived in a village and they went to the south of the country.

134
00:10:49,000 --> 00:10:52,000
I was here at school and I worked here in Amsterdam.

135
00:10:52,000 --> 00:10:55,000
So I couldn't go there.

136
00:10:55,000 --> 00:10:58,000
Then I went to live in a small room in the village.

137
00:10:58,000 --> 00:11:00,000
Then you had to look for a long time.

138
00:11:00,000 --> 00:11:03,000
Through friends and acquaintances you could get a small room.

139
00:11:03,000 --> 00:11:06,000
I paid more than 300 guilders a month for a room.

140
00:11:06,000 --> 00:11:09,000
Plus gas and electricity separately.

141
00:11:09,000 --> 00:11:12,000
I had a friend who lived in the neighborhood.

142
00:11:12,000 --> 00:11:16,000
He moved six times from a small room to a small room in Amsterdam.

143
00:11:16,000 --> 00:11:19,000
I had another friend who lived with his parents.

144
00:11:19,000 --> 00:11:21,000
We knew each other through school.

145
00:11:21,000 --> 00:11:24,000
Then we decided to crack in Amsterdam.

146
00:11:24,000 --> 00:11:27,000
See if we can find a suitable building.

147
00:11:27,000 --> 00:11:29,000
We wanted to live there with the three of us.

148
00:11:29,000 --> 00:11:32,000
We wanted to take a speculation building.

149
00:11:32,000 --> 00:11:36,000
The buildings were empty and they didn't do anything with it.

150
00:11:36,000 --> 00:11:40,000
One night we came across these six empty buildings.

151
00:11:40,000 --> 00:11:44,000
We went to the cracker and asked if they could help us crack it.

152
00:11:44,000 --> 00:11:49,000
At some point there were 40 or 50 people who wanted to crack it.

153
00:11:49,000 --> 00:11:52,000
With the help of people who had already cracked it.

154
00:11:52,000 --> 00:11:54,000
We were almost all experienced.

155
00:11:54,000 --> 00:11:56,000
We cracked it here on November 1, 1978.

156
00:11:56,000 --> 00:12:00,000
On behalf of the OGHAM, spy Paul van Wisse infiltrates.

157
00:12:00,000 --> 00:12:02,000
The judge demands exclusion.

158
00:12:02,000 --> 00:12:06,000
For the first time, the crackers decide to put a judge's fund next to them.

159
00:12:06,000 --> 00:12:09,000
They impose housing rights above property rights.

160
00:12:09,000 --> 00:12:13,000
At some point we decided that we wouldn't take it any longer.

161
00:12:13,000 --> 00:12:18,000
It's crazy that you've thrown away a house that's been empty for two years.

162
00:12:18,000 --> 00:12:20,000
We'll just stay here.

163
00:12:20,000 --> 00:12:23,000
We'll make it clear to everyone that we'll stay here.

164
00:12:23,000 --> 00:12:28,000
It's our intention that we don't act as prisoners, but as attackers.

165
00:12:28,000 --> 00:12:34,000
It's a mental change that we don't try to get rid of the police.

166
00:12:34,000 --> 00:12:42,000
It may sound strange, but that's what our whole action is based on.

167
00:12:42,000 --> 00:12:47,000
That we take the initiative and don't let them lead us to the slaughterhouse.

168
00:12:47,000 --> 00:12:50,000
For me it's a very fundamental matter.

169
00:12:50,000 --> 00:12:53,000
It's not like I let them drag me away and it's over.

170
00:12:53,000 --> 00:12:58,000
While the publicity is going well and the solidarity testimonies are flowing in...

171
00:12:58,000 --> 00:13:01,000
...the Grand Emperor is being quickly converted into a fort.

172
00:13:01,000 --> 00:13:05,000
The Grand Emperor becomes the symbol of tens of thousands of Amsterdam residents.

173
00:13:05,000 --> 00:13:10,000
It's not just about preserving housing, but also about setting a limit.

174
00:13:10,000 --> 00:13:13,000
The crackers prepare for a fierce battle.

175
00:13:13,000 --> 00:13:15,000
We're not going anywhere.

176
00:13:15,000 --> 00:13:18,000
Send a house and not even more offices.

177
00:13:18,000 --> 00:13:20,000
We're not going anywhere.

178
00:13:20,000 --> 00:13:25,000
Everyone! No, no, we're not going anywhere.

179
00:13:25,000 --> 00:13:28,000
No, no, we're not going anywhere.

180
00:13:28,000 --> 00:13:32,000
Send a house and not even more offices.

181
00:13:32,000 --> 00:13:35,000
We're not going anywhere.

182
00:13:35,000 --> 00:13:40,000
Good evening, this is Radio De Vrije Keizer on the 102 Megahertz FM.

183
00:13:40,000 --> 00:13:46,000
We're going to play some music, but I'd like to call on the people in Amsterdam who are listening to us.

184
00:13:46,000 --> 00:13:50,000
Do you have any blankets or records in stock?

185
00:13:50,000 --> 00:13:52,000
Bring them here, we need them badly.

186
00:13:52,000 --> 00:13:54,000
We only have three albums here.

187
00:13:54,000 --> 00:13:56,000
That's not enough.

188
00:13:57,000 --> 00:14:04,000
I'd like to thank the neighbor who just brought a bag of chips and a blanket.

189
00:14:04,000 --> 00:14:08,000
I'm going to play a record for him for the second time.

190
00:14:08,000 --> 00:14:18,000
I hope there are more people who follow this example.

191
00:14:18,000 --> 00:14:20,000
It was also a certain growth process.

192
00:14:20,000 --> 00:14:24,000
It's not like we're going to fight the police again.

193
00:14:24,000 --> 00:14:26,000
You've seen that with the Kinkenbuurt.

194
00:14:26,000 --> 00:14:30,000
At some point, people stood in front of the building and were beaten away.

195
00:14:30,000 --> 00:14:33,000
The crackers didn't do anything else, they just beat them away.

196
00:14:33,000 --> 00:14:39,000
This has happened several times. We've said, let's not fight anymore.

197
00:14:39,000 --> 00:14:44,000
Apparently, you have to use violence, otherwise you won't succeed.

198
00:14:44,000 --> 00:14:47,000
That's how I see it right now.

199
00:14:47,000 --> 00:14:51,000
I have the idea that you are slowly being driven in that direction.

200
00:14:51,000 --> 00:14:52,000
There's nothing else left to do.

201
00:14:52,000 --> 00:14:53,000
Talking doesn't help anymore.

202
00:14:53,000 --> 00:14:55,000
Your suffering doesn't help anymore.

203
00:14:55,000 --> 00:14:58,000
You have to show that you can hit back.

204
00:14:58,000 --> 00:15:03,000
It's a risk to listen to what you're actually trying to say.

205
00:15:03,000 --> 00:15:07,000
The problem with violence is not that it's a problem, but that it's not important anymore.

206
00:15:07,000 --> 00:15:18,000
You notice that you are forced to be much more principled.

207
00:15:18,000 --> 00:15:23,000
We've been talking for years, and that doesn't help.

208
00:15:23,000 --> 00:15:28,000
You have to occupy a building and put things on the map.

209
00:15:28,000 --> 00:15:32,000
You don't have to leave it with the first blow.

210
00:15:32,000 --> 00:15:36,000
Then nothing else happens.

211
00:15:36,000 --> 00:15:43,000
If you are going to resist things that are structurally wrong in our society,

212
00:15:43,000 --> 00:15:46,000
such as property relations, how to speculate with the house,

213
00:15:46,000 --> 00:15:51,000
how people who have a lot of money can invest their money in the house.

214
00:15:51,000 --> 00:15:57,000
If you try to do something about it, you get a lot of violence.

215
00:15:57,000 --> 00:16:00,000
For example, with the big emperor.

216
00:16:00,000 --> 00:16:05,000
In the end, I borrowed a helmet from someone,

217
00:16:05,000 --> 00:16:10,000
although I didn't feel comfortable wearing such a thing on my head.

218
00:16:10,000 --> 00:16:23,000
But at some point you realize that it is necessary, because otherwise they hit the most annoying places.

219
00:16:23,000 --> 00:16:27,000
Then you have to put such a thing on your head, although I don't want that at all.

220
00:16:27,000 --> 00:16:31,000
And then I had old clothes in the corner.

221
00:16:31,000 --> 00:16:37,000
Every time the phone rang early in the morning, I thought, now it's time.

222
00:16:37,000 --> 00:16:41,000
In Amsterdam, 54,000 people are looking for housing.

223
00:16:41,000 --> 00:16:43,000
Entire buildings are empty.

224
00:16:43,000 --> 00:16:45,000
Houses are being sold horizontally.

225
00:16:45,000 --> 00:16:47,000
Houses are being demolished.

226
00:16:47,000 --> 00:16:50,000
And big houses are being demolished.

227
00:16:50,000 --> 00:16:55,000
The mayor Pollak and the committee of BNW think that the big emperor should be removed.

228
00:16:55,000 --> 00:16:57,000
He is from the municipality of Amsterdam,

229
00:16:57,000 --> 00:17:02,000
who has ordered anti-speculation measures to be taken.

230
00:17:02,000 --> 00:17:04,000
We demonstrate against this.

231
00:17:04,000 --> 00:17:07,000
The housing shortage in Amsterdam should finally be tackled.

232
00:17:07,000 --> 00:17:11,000
Not removed, but counted for everyone.

233
00:17:11,000 --> 00:17:14,000
Red yes, red no.

234
00:17:14,000 --> 00:17:17,000
Red yes, red no.

235
00:17:17,000 --> 00:17:19,000
Red yes, red no.

236
00:17:19,000 --> 00:17:22,000
I still have to think about it in the last few days.

237
00:17:22,000 --> 00:17:26,000
At the beginning, there was a housing meeting of the people who had survived.

238
00:17:26,000 --> 00:17:27,000
There were ten men.

239
00:17:27,000 --> 00:17:31,000
And then the mood was like, well, it's hopeless and we're not going through.

240
00:17:31,000 --> 00:17:34,000
And then we decided to go through with those ten.

241
00:17:34,000 --> 00:17:37,000
It's unbelievable.

242
00:17:37,000 --> 00:17:39,000
How do you think it's going to be?

243
00:17:39,000 --> 00:17:43,000
I think it's because people have long been aware that they can't just live here.

244
00:17:43,000 --> 00:17:47,000
Everyone comes to realize what bad things are happening.

245
00:17:47,000 --> 00:17:50,000
You are kicked out because you don't want to pay rent.

246
00:17:50,000 --> 00:17:51,000
You live in a dump.

247
00:17:51,000 --> 00:17:54,000
You hear from people who live in a garage for 400 guilders per month.

248
00:17:54,000 --> 00:17:56,000
Who have nothing else, no gas, no electricity, nothing.

249
00:17:56,000 --> 00:17:57,000
It's really amazing.

250
00:17:57,000 --> 00:17:59,000
Those people see it all now.

251
00:17:59,000 --> 00:18:01,000
Everyone just comes in resistance. It just has to be different.

252
00:18:01,000 --> 00:18:03,000
Everyone just has to be able to live.

253
00:18:03,000 --> 00:18:07,000
Public contribution to tax evasion.

254
00:18:07,000 --> 00:18:09,000
Extreme mission.

255
00:18:09,000 --> 00:18:12,000
Public contribution to tax evasion.

256
00:18:12,000 --> 00:18:14,000
What does Rabani think of Udink?

257
00:18:14,000 --> 00:18:16,000
Udink is a moron.

258
00:18:16,000 --> 00:18:19,000
What does Moortmouw think of Anderhuizen?

259
00:18:19,000 --> 00:18:21,000
Anderhuizen is nothing.

260
00:18:21,000 --> 00:18:48,000
Anderhuizen is nothing.

261
00:18:48,000 --> 00:18:51,000
For the time being, the municipality does not dare to vacate the Grote Keizer.

262
00:18:51,000 --> 00:18:55,000
Crackers are being thrown around by the police in all possible ways.

263
00:18:55,000 --> 00:19:08,000
People who make innocent smoke bombs are arrested.

264
00:19:08,000 --> 00:19:13,000
The filling of a few bags of sand in defense of the Grote Keizer is marked as a serious crime.

265
00:19:13,000 --> 00:19:23,000
The police are massively evacuating.

266
00:19:23,000 --> 00:19:25,000
There was someone who was doing something there.

267
00:19:25,000 --> 00:19:26,000
He was picked up like that.

268
00:19:26,000 --> 00:19:28,000
When I come and look, I am also picked up.

269
00:19:28,000 --> 00:19:30,000
Yes, I have nothing to do with it.

270
00:19:30,000 --> 00:19:31,000
It's just picked up.

271
00:19:31,000 --> 00:19:32,000
Nothing to do with it.

272
00:19:32,000 --> 00:19:33,000
Nothing to do with it.

273
00:19:33,000 --> 00:19:34,000
Shall we go?

274
00:19:34,000 --> 00:19:36,000
Those police officers, what are they doing here?

275
00:19:36,000 --> 00:19:38,000
If they don't want to do anything.

276
00:19:38,000 --> 00:19:39,000
What do you think they should do?

277
00:19:39,000 --> 00:19:40,000
What should they do?

278
00:19:40,000 --> 00:19:45,000
Pick up all those pans for those big capitalists who have been smuggled out there.

279
00:19:45,000 --> 00:19:48,000
And Mr. Boersman is hanging in those rubbers.

280
00:19:48,000 --> 00:20:03,000
Yes, he sent us out.

281
00:20:03,000 --> 00:20:05,000
The Grote Keizer is left alone.

282
00:20:05,000 --> 00:20:20,000
But outside the rectory, the pans are emptied in the Safirstraat and the Tolstraat.

283
00:20:20,000 --> 00:20:48,000
Come on, come on, come on.

284
00:20:48,000 --> 00:21:00,000
Come on, come on, come on, come on.

285
00:21:00,000 --> 00:21:05,000
The series of evacuations, in which smugglers only offer painful resistance, ends at the Vondelstraat.

286
00:21:05,000 --> 00:21:09,000
The building, which has been empty for a year, is immediately emptied by the ME after the smuggling.

287
00:21:09,000 --> 00:21:13,000
The police choose for the umpteenth time party for the landowner.

288
00:21:13,000 --> 00:21:17,000
The joint Amsterdam smuggling groups decide not to pick it up anymore.

289
00:21:17,000 --> 00:21:22,000
Within a week, the building will be cracked again and the ME will come again.

290
00:21:22,000 --> 00:21:24,000
The patience of the smugglers is now over.

291
00:21:24,000 --> 00:21:50,000
With everything that is left for the smugglers, the ME is hit back.

292
00:21:50,000 --> 00:21:54,000
To prevent an evacuation, barricades are spontaneously placed.

293
00:21:54,000 --> 00:21:56,000
Hundreds of Amsterdammers come to help.

294
00:21:56,000 --> 00:22:20,000
In a few hours, the Vondelstraat will be evacuated.

295
00:22:20,000 --> 00:22:29,000
The building on the corner of the Vondelstraat and the first entrance to the Heugelstraat has been cracked by us to live in it.

296
00:22:29,000 --> 00:22:38,000
The barricades have been thrown up by us to defend us against illegal attacks by the police.

297
00:22:38,000 --> 00:22:48,000
If the municipality guarantees that we can continue to live in the building, then we no longer need the barricades.

298
00:22:48,000 --> 00:22:51,000
The simple demands of the smugglers are not met.

299
00:23:18,000 --> 00:23:45,000
A violence against the police is very difficult for me because I don't want to hurt anyone.

300
00:23:45,000 --> 00:23:53,000
It is very sad that you have to fight your fellow citizens to be able to live there.

301
00:23:53,000 --> 00:24:16,000
At the Vondelstraat, the ME forces try to overrun people.

302
00:24:16,000 --> 00:24:23,000
Through the firefighters at the Vondelstraat, smugglers have discovered that you can also hit back.

303
00:24:23,000 --> 00:24:26,000
At subsequent evacuations, smugglers continue to resist hard.

304
00:24:26,000 --> 00:24:34,000
Police and politicians are attacking this to shift the attention of the housing shortage to public order.

305
00:24:34,000 --> 00:24:36,000
The ME introduces new tactics.

306
00:24:36,000 --> 00:24:40,000
Tear gas and break gas must break the resistance of the smugglers.

307
00:24:40,000 --> 00:25:08,000
Arrest teams are deployed.

308
00:25:08,000 --> 00:25:28,000
Smugglers try to break the resistance of the smugglers.

309
00:25:28,000 --> 00:25:50,000
To set an example, a number of people are picked up and held for a long time.

310
00:25:50,000 --> 00:26:18,000
Smugglers try to break the resistance of the smugglers.

311
00:26:18,000 --> 00:26:21,000
Smugglers try to break the resistance of the smugglers.

312
00:26:48,000 --> 00:27:12,000
Smugglers try to break the resistance of the smugglers.

313
00:27:12,000 --> 00:27:25,000
Smugglers try to break the resistance of the smugglers.

314
00:27:25,000 --> 00:27:31,000
Because he helped to defend a building, Jojo disappeared for two months.

315
00:27:31,000 --> 00:27:57,000
Smugglers try to break the resistance of the smugglers.

316
00:27:57,000 --> 00:28:24,000
Smugglers try to break the resistance of the smugglers.

317
00:28:24,000 --> 00:28:49,000
Smugglers try to break the resistance of the smugglers.

318
00:28:49,000 --> 00:29:08,000
Smugglers try to break the resistance of the smugglers.

319
00:29:08,000 --> 00:29:37,000
Smugglers try to break the resistance of the smugglers.

320
00:29:37,000 --> 00:29:53,000
Smugglers try to break the resistance of the smugglers.

321
00:29:53,000 --> 00:30:21,000
Smugglers try to break the resistance of the smugglers.

322
00:30:21,000 --> 00:30:33,000
Smugglers try to break the resistance of the smugglers.

323
00:30:33,000 --> 00:30:52,000
Smugglers try to break the resistance of the smugglers.

324
00:30:52,000 --> 00:31:20,000
Smugglers try to break the resistance of the smugglers.

325
00:31:20,000 --> 00:31:45,000
Smugglers try to break the resistance of the smugglers.

326
00:31:45,000 --> 00:31:59,000
Smugglers try to break the resistance of the smugglers.

327
00:31:59,000 --> 00:32:27,000
Smugglers try to break the resistance of the smugglers.

328
00:32:27,000 --> 00:32:40,000
Smugglers try to break the resistance of the smugglers.

329
00:32:40,000 --> 00:33:07,000
Smugglers try to break the resistance of the smugglers.

330
00:33:07,000 --> 00:33:25,000
Smugglers try to break the resistance of the smugglers.

331
00:33:25,000 --> 00:33:45,000
Smugglers try to break the resistance of the smugglers.

332
00:33:45,000 --> 00:34:02,000
Smugglers try to break the resistance of the smugglers.

333
00:34:02,000 --> 00:34:22,000
Smugglers try to break the resistance of the smugglers.

334
00:34:22,000 --> 00:34:45,000
Smugglers try to break the resistance of the smugglers.

335
00:34:45,000 --> 00:35:07,000
Smugglers try to break the resistance of the smugglers.

336
00:35:07,000 --> 00:35:27,000
Smugglers try to break the resistance of the smugglers.

337
00:35:27,000 --> 00:35:47,000
Smugglers try to break the resistance of the smugglers.

338
00:35:47,000 --> 00:36:07,000
Smugglers try to break the resistance of the smugglers.

339
00:36:07,000 --> 00:36:27,000
Smugglers try to break the resistance of the smugglers.

340
00:36:27,000 --> 00:36:47,000
Smugglers try to break the resistance of the smugglers.

341
00:36:47,000 --> 00:37:07,000
Smugglers try to break the resistance of the smugglers.

342
00:37:07,000 --> 00:37:27,000
Smugglers try to break the resistance of the smugglers.

343
00:37:27,000 --> 00:37:47,000
Smugglers try to break the resistance of the smugglers.

344
00:37:47,000 --> 00:38:07,000
Smugglers try to break the resistance of the smugglers.

345
00:38:07,000 --> 00:38:27,000
Smugglers try to break the resistance of the smugglers.

