Woninginbraak is een ingrijpende gebeurtenis, maar het komt helaas regelmatig voor.
U kunt het inbrekers moeilijker maken.
De kans is groter dat ze uw woning overslaan als ze verwachten lang met inbreken bezig te zijn.
Goed hang- en sluitwerk is daarom belangrijk.
Waardevolle spullen uit het zicht opbergen helpt ook.
Net als de indruk wekken dat er iemand thuis is.
Een beproefde methode om in te breken is het intrappen van de voordeur.
Om dit te bemoeilijken kunt u uw deur verstevigen met een extra slot op kniehoogte.
Het openbreken of forceren van een deur of kozijn komt veel voor.
Goed hang- en sluitwerk bemoeilijkt dat sterk.
Een anti-inbraakstrip is een goede aanvullende maatregel.
Voor de voordeur is wel een combinatie met minimaal een bijzetslot op kniehoogte nodig.
Een brekeizer maakt dan geen kans.
Met slecht beslag kan een inbreker heel gemakkelijk de cilinder vrijmaken en deze daarna met een taang afbreken en verwijderen.
Ook met goedgekeurd beslag waarbij de cilinder uitsteekt kan dat nog steeds.
Deze methode heeft geen kans met goedgekeurd beslag waarbij de cilinder goed is opgesloten en dus niet uitsteekt.
Om de cilinder uit het slot te trekken maken sommige inbrekers gebruik van zelfgemaakt of professioneel gereedschap dat slotenmakers ook gebruiken.
Cilindertrekken kunt u voorkomen door beslag te monteren dat hier tegen bestand is.
Dat is beslag met een beschermplaatje dat voorkomt dat de cilinder kan worden verwijderd.
Een beproefde methode om in te breken is door hengelen via de brievenbus.
Inbrekers kunnen uw deur zo binnen enkele seconden open maken.
Tenzij u uw deur consequent op slot draait en de sleutel eruit haalt en veilig opbergt.
Flipperen is met een hard stuk plastic de dagsschoot indrukken om zo de deur te openen.
Door uw deur altijd op slot te draaien voorkomt u dat een inbreker kan flipperen.
De gelegenheid maakt de dief.
Openstaande ramen, deuren of balkondeuren maken het dieven wel heel gemakkelijk om binnen te komen.
Zeker als er ook nog een kliko of een trap klaar staat.
Wilt u toch luchten, doet u dat dan alleen als u zelf direct zicht op de deur of op het raam heeft.
