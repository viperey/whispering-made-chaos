1
00:00:00,000 --> 00:00:12,000
Woninginbraak is een ingrijpende gebeurtenis, maar het komt helaas regelmatig voor.

2
00:00:12,000 --> 00:00:15,000
U kunt het inbrekers moeilijker maken.

3
00:00:15,000 --> 00:00:21,000
De kans is groter dat ze uw woning overslaan als ze verwachten lang met inbreken bezig te zijn.

4
00:00:21,000 --> 00:00:25,000
Goed hang- en sluitwerk is daarom belangrijk.

5
00:00:25,000 --> 00:00:30,000
Waardevolle spullen uit het zicht opbergen helpt ook.

6
00:00:30,000 --> 00:00:35,000
Net als de indruk wekken dat er iemand thuis is.

7
00:00:42,000 --> 00:00:47,000
Een beproefde methode om in te breken is het intrappen van de voordeur.

8
00:00:47,000 --> 00:00:55,000
Om dit te bemoeilijken kunt u uw deur verstevigen met een extra slot op kniehoogte.

9
00:00:59,000 --> 00:01:06,000
Het openbreken of forceren van een deur of kozijn komt veel voor.

10
00:01:06,000 --> 00:01:09,000
Goed hang- en sluitwerk bemoeilijkt dat sterk.

11
00:01:09,000 --> 00:01:12,000
Een anti-inbraakstrip is een goede aanvullende maatregel.

12
00:01:12,000 --> 00:01:17,000
Voor de voordeur is wel een combinatie met minimaal een bijzetslot op kniehoogte nodig.

13
00:01:17,000 --> 00:01:21,000
Een brekeizer maakt dan geen kans.

14
00:01:25,000 --> 00:01:34,000
Met slecht beslag kan een inbreker heel gemakkelijk de cilinder vrijmaken en deze daarna met een taang afbreken en verwijderen.

15
00:01:34,000 --> 00:01:43,000
Ook met goedgekeurd beslag waarbij de cilinder uitsteekt kan dat nog steeds.

16
00:01:43,000 --> 00:01:53,000
Deze methode heeft geen kans met goedgekeurd beslag waarbij de cilinder goed is opgesloten en dus niet uitsteekt.

17
00:01:53,000 --> 00:02:13,000
Om de cilinder uit het slot te trekken maken sommige inbrekers gebruik van zelfgemaakt of professioneel gereedschap dat slotenmakers ook gebruiken.

18
00:02:23,000 --> 00:02:28,000
Cilindertrekken kunt u voorkomen door beslag te monteren dat hier tegen bestand is.

19
00:02:28,000 --> 00:02:33,000
Dat is beslag met een beschermplaatje dat voorkomt dat de cilinder kan worden verwijderd.

20
00:02:37,000 --> 00:02:42,000
Een beproefde methode om in te breken is door hengelen via de brievenbus.

21
00:02:42,000 --> 00:02:46,000
Inbrekers kunnen uw deur zo binnen enkele seconden open maken.

22
00:02:46,000 --> 00:02:53,000
Tenzij u uw deur consequent op slot draait en de sleutel eruit haalt en veilig opbergt.

23
00:02:56,000 --> 00:03:02,000
Flipperen is met een hard stuk plastic de dagsschoot indrukken om zo de deur te openen.

24
00:03:05,000 --> 00:03:10,000
Door uw deur altijd op slot te draaien voorkomt u dat een inbreker kan flipperen.

25
00:03:10,000 --> 00:03:12,000
De gelegenheid maakt de dief.

26
00:03:12,000 --> 00:03:18,000
Openstaande ramen, deuren of balkondeuren maken het dieven wel heel gemakkelijk om binnen te komen.

27
00:03:18,000 --> 00:03:22,000
Zeker als er ook nog een kliko of een trap klaar staat.

28
00:03:22,000 --> 00:03:38,000
Wilt u toch luchten, doet u dat dan alleen als u zelf direct zicht op de deur of op het raam heeft.

