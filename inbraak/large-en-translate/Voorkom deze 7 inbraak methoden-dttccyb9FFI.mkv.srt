1
00:00:00,000 --> 00:00:06,000
Housing interruption is a common occurrence, but unfortunately it occurs regularly.

2
00:00:06,000 --> 00:00:09,000
You can make it more difficult for interrupters.

3
00:00:09,000 --> 00:00:15,000
The chance is greater that they will leave your home if they expect to be busy with interruption for a long time.

4
00:00:15,000 --> 00:00:19,000
Good hanging and closing work is therefore important.

5
00:00:19,000 --> 00:00:25,000
Helping to store valuable items out of sight is also important.

6
00:00:25,000 --> 00:00:30,000
Helping to store valuable items out of sight is also important.

7
00:00:30,000 --> 00:00:34,000
Just like the impression that someone is at home.

8
00:00:42,000 --> 00:00:47,000
A proven method of interruption is stepping into the front door.

9
00:00:47,000 --> 00:00:59,000
To make this more difficult, you can reinforce your door with an extra lock at knee height.

10
00:00:59,000 --> 00:01:06,000
Breaking open or forcing a door or closet comes first.

11
00:01:06,000 --> 00:01:09,000
Good hanging and closing work makes it more difficult.

12
00:01:09,000 --> 00:01:12,000
An anti-interruption strip is a good additional measure.

13
00:01:12,000 --> 00:01:17,000
For the front door, a combination with at least an extra lock at knee height is necessary.

14
00:01:17,000 --> 00:01:20,000
A broken iron then makes no chance.

15
00:01:25,000 --> 00:01:33,000
With poor handling, an interrupter can easily remove the cylinder and then break it off with a tooth and remove it.

16
00:01:33,000 --> 00:01:41,000
This method has no chance with good handling, where the cylinder is well locked and therefore does not protrude.

17
00:02:03,000 --> 00:02:12,000
To pull the cylinder out of the lock, some interrupters use self-made or professional tools that lock makers also use.

18
00:02:23,000 --> 00:02:28,000
Pulling the cylinder can be prevented by mounting a coating that is resistant to this.

19
00:02:28,000 --> 00:02:33,000
That is coating with a protective sheet that prevents the cylinder from being removed.

20
00:02:38,000 --> 00:02:43,000
A proven method of interruption is hanging through the letterbox.

21
00:02:43,000 --> 00:02:46,000
Interruptors can open your door in a few seconds.

22
00:02:46,000 --> 00:02:52,000
Unless you consistently turn your door and remove the key and store it safely.

23
00:02:52,000 --> 00:02:57,000
Flipping is pressing the day shot with a hard piece of plastic to open the door.

24
00:03:01,000 --> 00:03:06,000
By always turning your door on lock, you prevent an interrupter from flipping.

25
00:03:12,000 --> 00:03:14,000
The opportunity makes the thief.

26
00:03:14,000 --> 00:03:20,000
Open windows, doors or balcony doors make it very easy for thieves to come in.

27
00:03:20,000 --> 00:03:24,000
Especially if there is also a cliquot or a staircase ready.

28
00:03:24,000 --> 00:03:43,000
Do you want to ventilate anyway? Do you only do that if you have direct sight of the door or the window?

